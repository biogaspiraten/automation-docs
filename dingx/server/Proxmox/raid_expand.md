#Erweitern des RAID
Erstellt von Hauke Czora am 22.08.2018
##Hinzufügen weiterer Festplatten
* Festplatten in den Server einbauen
* in der Konsole des Servers ´ssacli´ ausführen
* ´ctrl slot=2 ld 1 add drives=allunassigned´
* auf den Abschluss der Transformation des Arrays warten (dauert mindestens mehrere Stunden)
* ´ctrl slot=2 ld 1 modify size=max´
* ´exit´
* System rebooten
Nach dem Reboot solte Proxmox die neue Größe des Raid erkennen, gegebenenfalles muss noch die Partition angepasst werden.