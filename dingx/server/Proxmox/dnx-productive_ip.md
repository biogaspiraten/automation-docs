#IP-Konfiguration auf 10.10.100.25 (Ding.X Produktivsystem)
Erstellt von Hauke Czora am 30.11.2017
##Allgemeines
Der Server verwendet die Bionic Beaver standart tools, die IP-Konfiguration kann mit ´ip a´ ausgelesen werden. Die Konfigurationsdatei liegt zukünftig unter ´/etc/netplan/´. Bis dahin wird erstmal ´/etc/network/interfaces´ verwendet.
