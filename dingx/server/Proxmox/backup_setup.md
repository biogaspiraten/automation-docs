##Ein Backup für eine VM einrichten
Erstellt von Hauke Czora am 22.08.2018
###Eine weitere VM zusätzlich vom Hypervisor sichern
1. Auf dem Backupserver die Verzeichnisse für die Sicherungen anlegen:
  * in /backup/backup/
  * in /backup/lts/
  * in /home/ftpuser/ftproot/
  * die drei Verzeichnisse müssen den gleichen, einzigartigen Namen haben, dieser ist im Shellskript auf dem Hypervisor anzugeben
1. Auf dem Hypervisor die bkp.sh erweitern:
  * mit ´nano /home/northtec/bkp.bat´ die Datei öffnen
  * einen weiteren Aufruf des Bashskriptes einfügen und die Parameter konfigurieren
  * ´/home/northtec/bacup_routine.bash <vmid> <Verzeichnissname> <IP> <Benutzername> <Passwort>´
    * <vmid> ist die ID-Nummer der VM (wird in Proxmox angezeigt)
	* <Verzeichnissname> ist der Name der im ersten Schritt angelegten Verzeichnisse
	* <IP> ist die IP des Backup-FTP-Servers (10.10.100.30)
	* <Benutzername> ist der Benutzername zum Anmelden am FTP-Server (ftpuser)
	* <Passwort> ist das Passwort zum Anmelden am FTP-Server (ftpuser)
###Auf einem neuen Hypervisor die Sicherung einrichten
1. Auf dem Backupserver die Verzeichnisse für die Sicherungen anlegen:
  * in /backup/backup/
  * in /backup/lts/
  * in /home/ftpuser/ftproot/
  * die drei Verzeichnisse müssen den gleichen, einzigartigen Namen haben, dieser ist im Shellskript auf dem Hypervisor anzugeben
1. Auf dem Hypervisor die backup.bat erstellen:
  * mit ´nano /home/northtec/bkp.bat´ die Datei öffnen
  * ´/home/northtec/bacup_routine.bash <vmid> <Verzeichnissname> <IP> <Benutzername> <Passwort>´
    * <vmid> ist die ID-Nummer der VM (wird in Proxmox angezeigt)
	* <Verzeichnissname> ist der Name der im ersten Schritt angelegten Verzeichnisse
	* <IP> ist die IP des Backup-FTP-Servers (10.10.100.30)
	* <Benutzername> ist der Benutzername zum Anmelden am FTP-Server (ftpuser)
	* <Passwort> ist das Passwort zum Anmelden am FTP-Server (ftpuser)
1. als root ftp installieren (falls nicht vorhanden)
  * ´apt-get install ftp´
1. Mit ftp auf den Backupserver zugreifen (manuell!), um das Zertifikat zu speichern (sonst funktioniert die automatische Sicherung nicht):
  * ´ftp 10.10.100.30´
  * Benutzername: ftpuser
  * Passwort: ftpuser
1. Einen Cronjob für das Ausführen der Batchdatei anlegen (als root):
  * ´nano /etc/crontab´
  * folgende Zeile einfügen in die Tabelle einfügen (vor dem letzten #):
    * ´<m> <h> * * * root /home/northtec/bkp.sh´
	  * <m> durch die Minute ersetzen zu der das Backup gestartet werden soll (z.B. ´0´ zur vollen Stunde)
	  * <h> durch die Stunde ersetzen zu der das Backup gestartet werden soll (z.B. ´*/6´ alle 6 Stunden)
###Ein Backup mit einer E-Mail-Benachichtigung versehen
1. In der bkp.sh an die Zeile mit dem Bashskript ´ > <file>´ anhängen
  * <file> ist der Pfad zu einer Datei
2. an das Ende der bkp.sh ´mail -s <Betreff> <Adresse> < <file>´ anhängen
  * <Betreff> ist der Betreff der Mail
  * <Adresse> ist die E-Mail-Adresse an die die Mail geschickt werden soll
  * <file> ist der Phad zu der Datei aus Schritt 1.
