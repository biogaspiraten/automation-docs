#Gängige MongoDB Probleme beheben
Erstellt von Hauke Czora am 21.08.2018
##.lock-Dateien machen Probleme
Wenn mongod mit fehlercode 62 stoppt muss /var/lib/mongodb/mongod.lock gelöscht werden.  
Wenn mongod mit fehlercode 100 stoppt muss /var/lib/mongodb/wiredTiger.lock gelöscht werden.  
##Unsauberes Beenden kann die Datenbank töten
Ein plötzliches Beenden der VM kann zurfolge haben, dass mongod nicht mehr startet mit der Fehlermeldung (code=dumped, signal=ABRT), das bedeutet das die Dateien wiredTiger.wt und wiredTiger.turtle beschädigt sind und es erforderlich ist ein Backup einzuspielen.