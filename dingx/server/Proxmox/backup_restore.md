##Ein Backup wiederherstellen
Erstellt von Hauke Czora am 22.08.2018

1. Das Backup auf dem Backupserver verfügbar machen
  * am Backupserver anmelden
  * ´sudo cp /backup/backup/DNX-ubuntu/<Backup>.vma.gz /home/ftpuser/ftproot/´
  * ´sudo chown ftpuser: /home/ftpuser/ftproot/<Backup>.vma.gz´
1. Das Backup auf das Produktivsystem hohlen
  * am Produktivsystem anmelden
  * ´ftp 10.10.100.30´
  * Benutzername und Passwort: ftpuser
  * ´lcd /var/lib/vz/dump´
  * ´cd ftproot´
  * ´get <Backup>.vma.gz´
1. Das Backup einspielen
  * In Proxmox den Speicher 'local' auswählen, dort 'Content' auswählen
  * das übertragene Backup auswählen und 'Restore' klicken
  * gegebenen Falles den Anweisungen folgen