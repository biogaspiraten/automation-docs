#Proxmox installieren
Erstellt von Hauke Czora am 20.08.2018
##Installationsmedium erstellen
Soll ein Installations-USB-Stick verwendet werden, so kann dieser nicht mit Rufus erstellt werden, das Flashen mit Etcher funktioniert jedoch einwandfrei.
##Besonderheiten bei der Installation
Es ist nach Möglichkeit der endgültige Rechnername sowie die endgültige IP-Konfiguration vorzunehmen, da andernfalls später Probleme auftreten können. Es wird kein Benutzername abgefragt, sondern ein Standartbenutzer 'root' erstellt.