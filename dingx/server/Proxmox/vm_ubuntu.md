#Ubuntu-VM erstellen
Erstellt von Hauke Czora am 22.08.2018
1. Hardware in Proxmox erstellen
  * Im Webinterface des Zielhypervisors oben rechts 'Create VM' klicken
  * Die VM wie gewünscht konfigurieren (die einzige Änderung, die zwingend erforderlich ist, ist die Außwahl einer Installations-ISO)
  * mit 'Finish' wird die virtuelle Hardware erzeugt
  * die VM anklicken und in Hardware das Tastaturlayout auf 'German (DE)' stellen
1. Das Betriebssystem installieren
  * Falls noch nicht geschehen die Installations-ISO in das virtuelle Laufwerk einlegen
  * die Bootreihenfolge so verändern, dass zuerst vom CD-Laufwerk gebootet wird
  * die VM starten und die Shell aufrufen
  * das Betriebssystem mit dem Installationsassistenten installieren
    * Achtung! Bei Ubuntu 18.04 ist es erforderlich den LVM manuell zu vergrößern, falls das Rootverzeichnis größer als 4GiB sein soll.
1. Einen SSH-Server installieren
  * ´sudo apt-get install sshd´
  * die Standartkonfiguration ermöglicht eine Verbindung zum Server, es ist nicht zwingend erforderlich diese zu ändern.