#Proxmox Updates
Erstellt von Hauke Czora am 19.09.2018

* root-Rechte hohlen
* nano /etc/apt/sources.list.d/pve-enterprise.list
* erste Zeile (endet mit 'pve-enterprise') auskommentieren
* folgende Zeile einfügen
  * ´deb http://download.proxmox.com/debian stretch pve-no-subscription´
* Datei speichern und verlassen
* apt-get update
* apt-get dist-upgrade
