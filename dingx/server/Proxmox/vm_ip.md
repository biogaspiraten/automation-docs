##Die IP-Adresse einer VM auslesen
Erstellt von Hauke Czora am 22.08.2018
###Wo wird die IP gezeigt?
Die IP-Adresse einer laufenden VM wird in Proxmox im Tab 'Summary' angezeigt.  
###Konfiguration des Guest-Agent
* unter 'Options' bei 'Qemu Agent' das Häckchen setzen
* in der VM den Agenten installieren
  * in den Packetquellen muss universe aktiviert sein
    * ´sudo nano /etc/apt/sources.list´
	* falls nicht vorhanden folgende Einträge hinzufügen.
	  * ´deb http://archive.ubuntu.com/ubuntu bionic universe´
	  * ´deb http://archive.ubuntu.com/ubuntu bionic-updates universe´
  * ´sudo apt-get install qemu-guest-agent´