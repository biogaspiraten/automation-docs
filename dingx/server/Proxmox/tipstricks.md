##Tips und Tricks
Erstellt von Hauke Czora am 23.07.2018
### ´Alt Gr´ verwenden
Um in der noVNC-Konsole des Webinterfaces ´Alt Gr´ zu verwenden muss nach dem druck dieser Taster erst ´Strg´ gedrückt und losgelassen werden ehe die Taste, die modifiziert werden soll, gedrückt wird. Die xterm.js-Konsole unterstützt ´Alt Gr´ von sich aus. Alternativ kann in den VM-Einstellungen die Tastatur von 'Default' auf 'Deutsch' umgestellt werden.
### Verändern des Tastaturlayouts
´sudo apt-get install console-common´ zum Installieren des nötigen Programms.  
´sudo dpkg-reconfigure console-data´ zum Ändern des Tastaturlayouts.  
In hartneckigen Fällen ´sudo loadkeys /usr/share/keymaps/i386/qwertz/de-latin1-nodeadkeys.kmap.gz´
### Fehlerhaften Cluster in einen zumindest teilweise funktionalen Zustand versetzen
Mit root folgende Befehle ausführen:  
´systemctl stop pve-cluster´  
´/usr/bin/pmxcfs -l´  
Jetzt sollten die VMs booten und die Dateinen unter /etc/pve/ editierbar sein.