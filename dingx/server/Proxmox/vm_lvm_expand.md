#Erweitern der Speicherkapazität von Ubuntu
Erstellt von Hauke Czora am 10.07.2018
* neue Festplatte dem System hinzufügen oder kapazität einer Bestehenden erweitern
* ´fdisk <disk>´
  * <disk> ist der Pfad zur zu verwendenden Festplatte (z.B. "/dev/sdb")
* ´n´
  * neue Partition anlegen
* [Enter]
  * default Wert erstellt primäre Partition
* [Enter]
  * default Wert beginnt mit dem ersten freien Sektor
* [Enter]
  * default Wert endet mit dem letzten Sektor
* ´t´
* ´8e´
  * ändert den Typ der Partition zu "Linux LVM"
* ´w´
  * speichert die neue Partition
* ´sudo vgextend <vgname> <partition>´
  * <vgname> ist der name der Volumengruppe (kann mit ´sudo vgs´ gefunden werden; z.B. "DNX-ubuntu-vg")
  * <partition> ist der Pfad der Partition (z.B. "/dev/sdb1")
* ´sudo lvresize -L <newsize> <lv>´
  * <newsize> ist die neue Größe der Volumengruppe (also alte Größe plus größe der neuen Partition (die alte Größe kann vorher mit ´sudo lvs´ abgefragt werden), Beispiel für eine Größe: "170.00g")
  * <lv> ist der Pfad zum logischen Volumen (z.B. "/dev/DNX-ubuntu-vg/root")
* ´sudo resize2fs <mapper>´
  * <mapper> ist der Pfad zum Mapper des Dateisystems (z.B. "/dev/mapper/DNX--ubuntu--vg-root")
* ´df -h´ sollte jetzt die neue Größe des Dateisystems anzeigen