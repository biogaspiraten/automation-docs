##Serverliste und Zugangsdaten
Erstellt von Hauke Czora am 13.10.2017

|Server|Benutzername|Passwort|Verwendung|IP-Adresse|
|---|---|---|---|---|
|dnxpve01|[proxmox-master]|-|Hypervisor für Produktivsysteme|10.10.100.10|
|dnxpve02|[proxmox-master]|-|Hypervisor für den Backupserver|10.10.100.11|
|DNX-ubuntu|northtec|humpiedumpie|VM mit dem Dingx-Produktivsystem (läuft auf dnxpve01)|10.10.100.25|
|DNX-backup|northtec|humpiedumpie|VM-Backupserver (läuft auf dnxpve02)|10.10.100.30|
|ip-172-31-6-129|ubuntu|**_kein Passwort_**|remote AWS-Instanz für MQTT-Broker (Login via private Key)|broker1.dingx.net|
|ip-172-31-45-131|ubuntu|**_kein Passwort_**|remote AWS-Instanz für MQTT-Broker (Login via private Key)|broker2.dingx.net|
|ip-172-31-0-230|ubuntu|**_kein Passwort_**|remote AWS-Instanz für Kamerarelais (Login via private Key)|cctv.dingx.net|
|proxmox-master|root|humpiedumpie|Proxmox-Hypervisor|nicht per SSH; Webinterface oder su verwenden|
|proxmox-master|northtec|nt#007|Proxmox-Hypervisor|Login für das Webinterface so wie SSH|