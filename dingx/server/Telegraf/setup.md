#Telegraf Setup auf Ubuntu
Erstellt von Hauke Czora am 23.11.2017

1. Download und Installation des Pakets
	* `wget https://dl.influxdata.com/telegraf/releases/telegraf_1.4.4-1_amd64.deb`
	* `sudo dpkg -i telegraf_1.4.4-1_amd64.deb`
2. Anpassen der Konfiguration
	* `sudo nano /etc/telegraf/telegraf.conf`
	* die Standartconfig enthält sehr viele Kommentare und ist selbsterklärend, die meißten In- und Outputs sind jedoch standartmäßig auskommentiert (Alles nach `#` ist ein Kommentar) 
	* falls bei der Installation keine telegraf.conf erzeugt wird kann dies mit `telegraf config > /etc/telegraf/telegraf.conf` nachgehohlt werden; Dieser Befehl erzeugt eine Standartconfig.
