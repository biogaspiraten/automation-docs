#Telegraf Kompilieren
Erstellt von Hauke Czora am 23.11.2017

1. Go herunterladen und installieren (keines Falles das Ubuntupacket 'golang' aus der Paketverwaltung benuzen, es ist hoffnungslos veraltet)
	* neuste Version von GO von `https://golang.org/dl/`
	* `wget -c https://storage.googleapis.com/golang/go1.9.2.linux-amd64.tar.gz`
	* `tar -C /usr/local -xzf go1.9.2.linux-amd64.tar.gz`
	* `nano .profile` am Ende der Datei folgende Zeilen einfügen
		* `export PATH=$PATH:/usr/local/go/bin`
		* `export GOROOT=/usr/local/go/`
		* `export GOPATH=$HOME/go/`
		* `export GOBIN=$HOME/go/bin/`
	* `mkdir go`
	* `sudo apt-get install make`
2. Das Telegraf-Repository herunterladen
	* `cd go`
	* `go get -d github.com/influxdata/telegraf`
3. Telegraf kompilieren
	* `cd ~/go/src/github.com/influxdata/telegraf`
	* `make`
	* mit der im aktuellen Verzeichnis befindlichen telegraf Binary die alte telegraf Binary (in `/usr/bin/`) ersetzen