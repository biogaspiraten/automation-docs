#.ppk-Dateien zur Authentifizierung in Putty verwenden
Erstellt von Hauke Czora am 23.11.2017

1. Putty starten
2. im Feld `Host Name (or IP address)` die Addresse des Ziel Host eingeben
3. die Kategorie `Connection` öffnen
4. die Subkategorie `SSH` öffnen
5. die Subkategorie `Auth` auswählen
6. den Pfad .ppk-Datei mit dem privaten Schlüssel des Servers in das Textfeld unter `Private key file for authentication:` eintragen
	* alternativ die Datei mittels der `Browse...` Schaltfläche auswählen
