#Telegraf Dokumentation
Erstellt von Hauke Czora am 23.11.2017
##Inhalt
* [Telegraf Setup](/dingx/server/Telegraf/setup.md)
* [Mehrere Telegraf-Instanzen per MQTT verbinden](/dingx/server/Telegraf/mqttLink.md)
* [PM2 Metriken an Telegraf übergeben](/dingx/server/Telegraf/pm2Anbindung.md)
* [Ordnergröße mit Telegraf auslesen](/dingx/server/Telegraf/dirstat.md)
* [Telegraf aus dem Quellcode kompilieren](/dingx/server/Telegraf/compile.md)
* [Putty Private Key verwenden](/dingx/server/Telegraf/ppkVerwenden.md)
