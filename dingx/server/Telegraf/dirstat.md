#Ordnergröße mit Telegraf auslesen
Erstellt von Hauke Czora am 23.11.2017

Das Auslesen der Gesammtgröße eines Verzeichnisses wird von Telegraf nicht nativ unterstützt, daher wurde ein eigenes Go-Plugin geschrieben. Das Plugin heißt 'dirstat' und basiert auf dem Plugin 'filestat' sowie Code von Stackowerflow. Prinzipiel macht es das gleiche wie 'filestat', nur mit dem Unterschied, dass es als Größe nicht die eigene Größe des Verzeichnisses, sondern die Summe aller Dateigrößen darin zurückgibt.

Um den Quellcode des Plugins hinzuzufügen, muss zwischen dem 2. und 3. Schritt der [Anleitung zum Kompilieren](/dingx/server/Telegraf/compile.md) noch folgendes erledigt werden:

* `cd ~/go/src/github.com/influxdata/telegraf/plugins/inputs`
* `mkdir dirstat`
* `cd dirstat`
* `nano dirstat.go`
* den Quellcode einfügen und speichern
* `cd ../all`
* `nano all.go`
* die Zeile `_ "git.com/influxdata/telegraf/plugins/inputs/dirstat` einfügen und speichern
	
##Quellcode
```go
	package dirstat

	import (
		"path/filepath"
		"log"
		"os"
	
		"github.com/influxdata/telegraf"
		"github.com/influxdata/telegraf/internal/globpath"
		"github.com/influxdata/telegraf/plugins/inputs"
	)
	
	const sampleConfig = `
	## Directories to gather stats about.
	##
	dirs = ["/var/log/**.log"]
	`
	
	type DirStat struct {
		Dirs []string
	
		// maps full file paths to globmatch obj
		globs map[string]*globpath.GlobPath
	}
	
	func NewDirStat() *DirStat {
		return &DirStat{
			globs: make(map[string]*globpath.GlobPath),
		}
	}
	
	func (_ *DirStat) Description() string {
		return "Read stats about given file(s)"
	}
	
	func (_ *DirStat) SampleConfig() string { return sampleConfig }
	
	func (f *DirStat) Gather(acc telegraf.Accumulator) error {
		var err error
	
		for _, dirpath := range f.Dirs {
			// Get the compiled glob object for this dirpath
			g, ok := f.globs[dirpath]
			if !ok {
				if g, err = globpath.Compile(dirpath); err != nil {
					acc.AddError(err)
					continue
				}
				f.globs[dirpath] = g
			}
	
			dirs := g.Match()
			if len(dirs) == 0 {
				acc.AddFields("dirstat",
					map[string]interface{}{
						"exists": int64(0),
					},
					map[string]string{
						"file": dirpath,
					})
				continue
			}
	
			for fileName, fileInfo := range dirs {
				tags := map[string]string{
					"file": fileName,
				}
				fields := map[string]interface{}{
					"exists": int64(1),
				}
	
				if fileInfo == nil {
					log.Printf("E! Unable to get info for file [%s], possible permissions issue",
						fileName)
				} else {
					var size int64
					filepath.Walk(fileName, func(_ string, info os.FileInfo, err error) error {
						if !info.IsDir() {
							size += info.Size()
						}
						return err
					})
					fields["size_bytes"] = size
					fields["modification_time"] = fileInfo.ModTime().UnixNano()
				}
	
				acc.AddFields("dirstat", fields, tags)
			}
		}
	
		return nil
	}
	
	func init() {
		inputs.Add("dirstat", func() telegraf.Input {
			return NewDirStat()
		})
	}
```
