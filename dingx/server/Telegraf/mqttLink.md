#Mehrere Telegraf-Instanzen per MQTT verbinden
Erstellt von Hauke Czora am 23.11.2017

Vorraussetzungen:

Mindestens 2 Telegraf-Instanzen, 1 MQTT-Broker

Umsetzung:

Die Telegraf-Instanz, die ihre Daten weitergeben soll (Sender) wird mit `[[outputs.mqtt]]` konfiguriert, unter `servers` wird der MQTT-Broker eingetragen, und wahrscheinlich müssen Zugangsdaten mitgegeben werden. Das `topic_prefix` sollte nur von Telegraf verwendet werden; Dieses und das `data_format` muss man sich merken, diese Informationen werden auch beim Empfänger benötigt.
Die Telegraf-Instanz, die empfangen soll (Empfänger) benötigt einen `[[inputs.mqtt_consumer]]`. Unter `servers` ist wieder der MQTT-Broker einzutragen, das `data_format` muss dem des Senders entspechen. Unter `topics` werden die zu abonierenden MQTT-Topics eingetragen, am einfachsten erhält man alle vom Sender übertragenen Metriken, in dem man das `topic_prefix` des Senders gefolgt von `/#` aboniert. Die Metriken stehen dann in InfluxDB unter dem Mesurment mit dem Host-Tag zur Verfügung.
