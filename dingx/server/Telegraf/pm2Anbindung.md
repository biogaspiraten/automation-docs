#PM2 Metriken an Telegraf
Erstellt von Hauke Czora am 23.11.2017

Telegraf liefert kein natives Plugin zum auslesen von PM2-Metriken und PM2 stellt nur eine JavaScript-Schnittstelle zur Verfügung um Metriken auszulesen. Daher wurde ein eigener Node-Dienst geschrieben, der über die API die PM2-Metriken ausließt und über einen UNIX-Socket als Datagramm an Telegraf übergibt.

Der Dienst benötigt neben der Node-Standartbibliothek noch die Pakete 'pm2' und 'unix-dgram' (ersteres für die PM2-API, letzters, da Node keine UNIX-Sockets mit Datagrammunterstützung mehr nativ zur Verfügung stellt (Telegraf selbst hat einen allgemeinen Socket-Input, welcher verschiedene Arten von Sockets verarbeitet)).


##Socket-Listener in telegraf.conf
```
	 # Generic socket listener capable of handling multiple socket types.
	 [[inputs.socket_listener]]
	   ## URL to listen on
	    service_address = "unixgram:///tmp/pm2_telegraf.sock"
	
	   ## Maximum socket buffer size in bytes.
	   ## For stream sockets, once the buffer fills up, the sender will start backing up.
	   ## For datagram sockets, once the buffer fills up, metrics will start dropping.
	   ## Defaults to the OS default.
	    read_buffer_size = 65535
	
	   ## Data format to consume.
	   ## Each data format has its own unique set of configuration options, read
	   ## more about them here:
	   ## https://github.com/influxdata/telegraf/blob/master/docs/DATA_FORMATS_INPUT.md
	    data_format = "json"
	    tag_keys = [ "name" ]
```

##Quellcode
```javascript
	var dgram = require("unix-dgram");
	var pm2 = require("pm2");
	
	var collector;
	pm2.connect(function(err) {
		if (err) {
			console.log(err);
			process.exit(2);
		}
		collector = setInterval(gatherMetrics, 10000);
	});
	
	process.on('SIGINT', function() {
	    clearInterval(collector);
		pm2.disconnect();
		process.exit(0);
	});
	
	function gatherMetrics() {
		pm2.list(function(err, processList) {
			var l = processList.length;
			var sock = dgram.createSocket("unix_dgram");
			for (var i = 0; i < l; i++) {
				var obj = {};
				obj.name = processList[i].name;//tag
				obj.pid = processList[i].pid;//value
				obj.memory = processList[i].monit.memory;//value
				obj.cpu = processList[i].monit.cpu;//value
				obj.instances = processList[i].pm2_env.instances;
				obj.restarts = processList[i].pm2_env.restart_time;//looks like time of last restart, but is nuber of restarts
				switch(processList[i].pm2_env.status) {
					case 'online':
						obj.state = 1;
						break;
					case 'stopping':
						obj.state = 3;
						break;
					case 'stopped':
						obj.state = 0;
						break;
					case 'launching':
						obj.state = 6;
						break;
					case 'errored':
						obj.state = -1;
						break;
					case 'one-launch-status':
						obj.state = 2;
						break;
					default:
						obj.state = -9;//value -9 means that no valid state was found
						break;
				}
				var msg = Buffer.from(JSON.stringify(obj));
				sock.send(msg, 0, msg.length, "/tmp/pm2_telegraf.sock");
			}
			sock.close();
		});
	}
	
```
