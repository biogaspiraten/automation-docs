#IP-Konfiguration auf 192.168.222.25 (Ding.X Produktivsystem)
Erstellt von Hauke Czora am 30.11.2017
##Allgemeines
Der Server verwendet NetworkManager, das heißt die Datei `/etc/network/interfaces` wird nicht verwendet. Die Konfiguration ist stadtdessen in der Datei `/etc/NetworkManager/system-connections/Kabel001` gespeichert; diese kann mit root-Rechten oder per nmcli angepasst werden.
