##Ein Backup wiederherstellen
Erstellt von Hauke Czora am 13.10.2017

1. Den PC so konfigurieren, dass Hyper-V-Manager mit dem Remoteserver verbunden werden kann
  - auf den Startbutton klicken und `cmd` eingeben
  - die App `Eingabeaufforderung` rechtsklicken und `Als Administrator ausführen` auswählen
    + folgende Komandos eingeben und jeweils mit der Enter-Taste zu bestätigen
      + `powershell`
      + `Set-Item WSMan:\localhost\Client\TrustedHosts -Value DNX-HyperV-1`
      + `Enable-WSManCredSSP -Role client -DelegateComputer DNX-HyperV-1`
    + auf den Startbutton klicken und `gpedit` eingeben und mit der Enter-Taste bestätigen
      + unter `Computerkonfiguration` auf den Pfeil vor `Administrative Vorlagen` klicken
      + auf den Pfeil vor `System` klicken
      + auf `Delegierung von Anmeldeinformationen` klicken
      + auf `Delegierung von aktuellen Anmeldeinformationen mit reiner NTLM-Serverauthentifizierung zulassen` doppelklicken
        + unter `Server zur Liste hinzufügen` auf `Anzeigen...` klicken
        + Folgenden Eintrag an die Liste anfügen
        + `wsman/DNX-HyperV-1.north-tec.local`
        + auf `OK` klicken
        + `Aktiviert` anwählen
        + auf `Übernehmen` klicken
      + den `Editor für lokale Gruppenrichtlienien` schließen
1. Hyper-V-Manager mit dem Remoteserver verbinden
  - Hyper-V-Manager öffnen
  - Im Aktionen-Tab (rechte Seite) auf 'Verbindung mit dem Server herstellen...' klicken
    + im Feld `Anderer Computer` `DNX-HyperV-1` eingeben
    + bei `Verbindung als anderer Benutzer herstellen` das Häckchen setzen
    + auf `Benutzer einstellen...` klicken
      + im Feld Benutzername `DNX-HyperV-1\Administrator` eingeben
      + im Feld Kennwort `Ab123456` eingeben
    + mit 'OK' bestätigen und erneut mit `OK` bestätigen
1. Die alte VM entfernen
  - im Hyper-V-Manager-Tab (linke Seite) auf `DNX-HyperV-1` klicken
  - in der Mitte die VM rechtsklicken und `Umbenennen...` auswählen
    + `_alt` oder Ähnliches an den VM-Namen anhängen
  - in der Mitte die VM rechtsklicken und `Ausschalten...` auswählen
    + Mit `Ausschalten` bestätigen
  - in der Mitte die VM rechtsklicken und `Löschen...` auswählen
    + Mit `Löschen` bestätigen
1. Mit Putty und FileZilla das Backup vom Backupserver hohlen
  - Putty öffnen
  - Im Feld `Host Name (or IP address)` `192.168.222.62` eingeben
  - Unter `Connection type` `SSH` auswählen
  - mit `Open` die Verbindung herstellen
    + in der Konsole den Benutzernamen `northtec` eingeben und mit der Enter-Taste bestätigen
    + in der Konsole das Passwort `humpiedumpie` eingeben und mit der Enter-Taste bestätigen
    + Ein passendes Backup auswählen, diese befinden sich in den Verzeichnissen
      + /backup/backup/DNX-HyperV-1/
      + /backup/lts/DNX-HyperV-1/
      + zum Kopieren des Backups in das FTP-Verzeichniss
      + `sudo cp /backup/<backup od. lts>/DNX-HyperV-1/<Backupbezeichnung>.7z /home/ftpuser/ftproot`
    + eingeben und mit Enter-Taste bestätigen
    + zum Beenden der Konsole `exit` eingeben und mit der Enter-Taste bestätigen
  - Einen USB-Stick mit ausreichend Speicherkapazität an den lokalen Rechner anschließen
  - FileZilla öffnen
    + im Feld 'Server' `192.168.222.62` eingeben
    + im Feld 'Benutzername' `ftpuser` eingeben
    + im Feld 'Passwort' `ftpuser` eingeben
    + im Feld 'Port' `22` eingeben
    + auf 'Verbinden' klicken
    + auf der lokalen Seite (links) zum USB-Stick navigieren
    + auf der server Seite (rechts) auf das Unterferzeichnis 'ftproot' klicken
    + die Datei  <Backupbezeichnung>.7z  vom Server auf den USB-Stick ziehen
    + nach Ende der Dateiübertragung FileZilla schließen
  - den USB-Stick entfernen
1. Das Backup auf den Hypervisor kopieren und entpacken
  - Den USB-Stick an den Remoteserver anschließen (dieser befindet sich im Serverraum)
  - auf den Startbutton klicken und `rdp` eingeben und mit der Enter-Taste bestätigen
    + in das Feld 'Computer' `DNX-HyperV-1` eingeben
    + auf 'Verbinden' klicken
    + das hintere Eingabeprompt (schwarzer Hintergrund) auswählen
      + Vorrausgesetzt das Backup liegt unter  E:\<Backupbezeichnung>.7z sind folgende Komandos einzugeben und jeweils mit der Enter-Taste zu bestätigen
        + `del C:\Users\Public\Documents\Hyper-V\Virtual Hard Disks\<Bezeichnung der virtuellen Festplatte>.vhdx`
        + `mkdir C:\import`
        + `cd C:\import`
        + `copy E:\<Backupbezeichnung>.7z C:\import`
        + `"C:\Program Files\7za.exe" x C:\import\<Backupbezeichnung>.7z`
      + warten bis das letzte Komando erfolgreich ausgeführt wurde
    + die RDP-Verbindung minimieren und zum Hyper-V-Manager wechseln
  - den USB-Stick entfernen
1. Das Backup importieren
  - im Hyper-V-Manager-Tab (linke Seite) auf 'DNX-HyperV-1' klicken
  - Im Aktionen-Tab (rechte Seite) auf 'Virtuellen Computer importieren...' klicken
    + Auf 'Ordner suchen' klicken
    + auf 'Durchsuchen' klicken und nach C:\import\<VM-Name> navigieren
    + auf 'Ordner auswählen' klicken
    + auf 'Weiter >' klicken
    + auf 'Weiter >' klicken
    + unter 'Wählen Sie den auszuführenden Importtyp aus' 'Virtuellen Computer kopieren' anwählen
    + auf 'Weiter >' klicken
    + auf 'Weiter >' klicken
    + auf 'Weiter >' klicken
    + auf 'Fertig stellen' klicken
  - in der Mitte die VM rechtsklicken und 'Starten' auswählen
  - Hyper-V-Manager schließen und zur RDP-Verbindung wechseln
  - `rd /s /q C:\import` eingeben und mit der Enter-Taste bestätigen
  - die RDP-Verbindung beenden
