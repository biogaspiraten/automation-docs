##Ubuntu VM aufsetzen
Erstellt von Hauke Czora am 17.10.2017

1. Im Hyper-V-Manager die Hardware erstellen
	* Einen verbundenen Hypervisor auswählen
	* ganz Rechts auf `Neu` `Virtuelle Computer...` klicken
	* Dem Installationswizzard folgen
		* Einen _EINDEUTIGEN_ Namen für die VM vergeben
		* `Generation 2` auswählen
		* Arbeitsspeicher zuweisen (Defaultwert kann zunächst übernommen werden)
		* Netzwerkferbindung herstellen
		* Eine virtuelle Fetplatte erzeugen (50GB reichen für den Anfang, 20GB sind für die Installation bereits genug), diese kann bei Bedarf später vergößert werden.
		* ISO-Datei des Ubuntu-Installationsmediums einbinden
2. Im Hyper-V-Manager die VM so konfigurieren, dass Ubuntu installiert und gestartet werden kann
	* Die VM rechtsklicken und `Einstellungen...` auswählen
	* Im Untermenü 'Sicherheit' 'Sicherer Start' deaktivieren
	* Gegebenenfalls die Einstellungen im Untermenü 'Arbeitsspeicher' anpassen
	* Gegebenenfalls die Einstellungen im Untermenü 'Prozessor' anpassen
3. Ubuntu installieren
	* Die VM rechtsklicken und `Verbinden...` auswählen
	* Den Button `Starten` klicken
	* Dem Ubuntu-Installationswizzard folgen
	* Nach Abschluss der Installation `sudo apt-get install sshd` eingeben, um einen SSH-Tunnel zu installieren
	* `ifconfig` eingeben um die IP-Addresse auszulesen (oder ggf. eine statische IP konfigurieren)
4. Mit Putty per SSH verbinden um die Integrationsdienste zu installieren
	* Alles updaten und die Integrationsdienste installieren
		* `sudo apt-get update`
		* `sudo apt-get upgrade`
		* `sudo apt-get dist-upgrade`
		* `sudo apt-get autoremove`
		* `sudo apt-get install linux-virtual-xenial`
		* `sudo apt-get install linux-tools-virtual-xenial linux-cloud-tools-virtual-xenial`
		* `sudo nano /etc/default/grub`
			* Die Zeile `GRUB_CMDLINE_LINUX_DEFAULT=""` durch `GRUB_CMDLINE_LINUX_DEFAULT="elevator=noop"` ersetzen
			* nano mit `strg + X` beenden und mit `Y` das speichern der Datei bestätigen
		* `sudo update-grub`
		* `sudo reboot`
