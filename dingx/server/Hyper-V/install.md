##Hyper-V installieren
Erstellt von Hauke Czora am 13.10.2017
###Installation auf Serverhardware
Die grundlegenden Installationsschritte sind identisch mit denen jeder anderen aktuellen Windowsversion, mit der Ausnahme, dass kein Productkey eingegeben werden muss.
* Installationsmedium an das Gerät anschließen/einlegen
* Im BIOS/UEFI einstellen, dass vom Installationsmedium gebootet werden soll (in der Regel wird das BIOS/UEFI mit einer der folgenden Tasten aufgerufen: `F1`, `F2`, `F11`, `F12` oder `Esc` es kann jedoch auch eine ganz Andere sein)
* Den Anweisungen des Installationsassistenten folgen.
Konfiguration für Remotezugriff:
1. IP-Konfiguration: Option 8 im Konfigurationsprompt, eine statische IP ist dringend empfolen
2. Rechnername: Option 1 im Konfigurationsprompt, muss einzigartig sein (insbesondere wenn das System in eine Domäne soll)
3. Remotezugriff: Optionen 4 und 7, bevor man das System headless macht, ist zu prüfen ob der Remotezugriff funktioniert

###Aktivieren unter Windows 10 pro
1. Starten der Powershell als Administrator
	* Auf den Startbutton klicken
	* `powershell` eingeben
	* Die Anwendung `Windows PowerShell` rechtsklicken und `Als Administrator ausführen` wählen
2. `Enable-WindowsOptionalFeature -Online -FeatureName:Microsoft-Hyper-V -All` eingeben und Enter drücken
3. Den PC neustarten
