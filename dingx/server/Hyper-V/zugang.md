##Serverliste und Zugangsdaten
Erstellt von Hauke Czora am 13.10.2017

|Server|Benutzername|Passwort|Verwendung|IP-Adresse|
|---|---|---|---|---|
|DNX-HyperV-1|Administrator|Ab123456|Hypervisor für Produktivsysteme|192.168.222.60|
|DNX-HyperV-2|Administrator|Ab123456|Hypervisor für den Backupserver|192.168.222.61|
|DNX-ubuntu|northtec|humpiedumpie|VM mit dem Dingx-Produktivsystem (läuft auf DNX-HyperV-1)|192.168.222.25|
|DNX-backup|northtec|humpiedumpie|VM-Backupserver (läuft auf DNX-HyperV-2)|192.168.222.62|
|DNX-Marketing|northtec|humpiedumpie|VM mit der Marketingwebsite für Dingx (läuft auf DNX-HyperV-1)|192.168.222.63 192.168.66.15|
|ip-172-31-6-129|ubuntu|**_kein Passwort_**|remote AWS-Instanz für MQTT-Broker (Login via private Key)|broker.dingx.net|
|ip-172-31-0-230|ubuntu|**_kein Passwort_**|remote AWS-Instanz für Kamerarelais (Login via private Key)|cctv.dingx.net|