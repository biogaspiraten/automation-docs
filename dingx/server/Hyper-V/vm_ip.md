##Die IP-Adresse einer VM auslesen
Erstellt von Hauke Czora am 13.10.2017

Die IP-Adresse einer laufenden VM wird im Hyper-V-Manager im Tab 'Netzwerk' angezeigt.  
Alternativ kann man auch im Hypervisor Powershell starten und folgenden Befehl eingeben `Get-VM | ?{$_.ReplicationMode -ne “Replica”} | Select -ExpandProperty NetworkAdapters | Select VMName, IPAddresses, Status`  
Anschließend kann die Powershell mit `exit` wieder verlassen werden.