##Hyper-V mit dem Manager verbinden
Erstellt von Hauke Czora am 13.10.2017
###ACHTUNG!
Der Hyper-V-Server kann nur mit einem Hyper-V-Manager auf einem kompatiblen Windowsbetriebssystem verbunden werden. Hyper-V 2016 erfordert Windows 10; Hyper-V 2012R2 erfordert Windows 8.1.

###Verbinden des Hypervisors mit Hyper-V-Manager
1. Den Hypervisor konfigurieren
	* Eine PowerShell mit Administratorrechten öffnen
	* Folgende Befehle ausführen:
		* `Enable-PSRemoting`
		* `Enable-WSManCredSSP -Role server`
2. Auf dem für das Management vorgesehenen PC den Hyper-V-Manager installieren
	* auf den Startbutton klicken und `powershell` eingeben
	* die App `Windows PowerShell` rechtsklicken und `Als Administrator ausführen` auswählen, dann folgenden Befehl eingeben
		* `add-windowsfeature rsat-hyper-v-tools`
3. Den PC so konfigurieren, dass Hyper-V-Manager mit dem Remoteserver verbunden werden kann
	* auf den Startbutton klicken und `powershell` eingeben
	* die App `Windows PowerShell` rechtsklicken und `Als Administrator ausführen` auswählen, dann folgende Befehle eingeben
		* `Set-Item WSMan:\localhost\Client\TrustedHosts -Value DNX-HyperV-1`
		* `Enable-WSManCredSSP -Role client -DelegateComputer DNX-HyperV-1`
	* auf den Startbutton klicken und `gpedit` eingeben und mit der Enter-Taste bestätigen
		* unter `Computerkonfiguration` auf den Pfeil vor `Administrative Vorlagen` klicken
		* auf den Pfeil vor `System` klicken
		* auf `Delegierung von Anmeldeinformationen` klicken
		* auf `Delegierung von aktuellen Anmeldeinformationen mit reiner NTLM-Serverauthentifizierung zulassen` doppelklicken
			* unter `Server zur Liste hinzufügen` auf `Anzeigen...` klicken
			* Folgenden Eintrag an die Liste anfügen
				* `wsman/DNX-HyperV-1.north-tec.local`
			* auf `OK` klicken
			* `Aktiviert` anwählen
			* auf `Übernehmen` klicken
		* den `Editor für lokale Gruppenrichtlienien` schließen
