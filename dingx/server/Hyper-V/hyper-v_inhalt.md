#Hyper-V Dokumentation
Erstellt von Hauke Czora am 13.10.2017
##Inhalt
* [Server und Zugangsdaten](/dingx/server/Hyper-V/zugang.md)
* [Hyper-V Istallieren](/dingx/server/Hyper-V/install.md)
* [Hyper-V mit Manager verbinden](/dingx/server/Hyper-V/connect.md)
* [Ubuntu VM aufsetzen](/dingx/server/Hyper-V/vm_ubuntu.md)
* [IP-Addresse einer VM bestimmen](/dingx/server/Hyper-V/vm_ip.md)
* [Backup einrichten](/dingx/server/Hyper-V/backup_setup.md)
* [Backup wiederherstellen](/dingx/server/Hyper-V/backup_restore.md)
* [IP-Konfiguration auf Ding.X-Produktivserver](/dingx/server/Hyper-V/dnx-productive_ip.md)
* [Hyper-V Remoteadministration mit RDP und Powershell](/dingx/server/Hyper-V/powershell.md)
* [Erweitern des RAID](/dingx/server/Hyper-V/raid_expand.md)
* [Eine neue VHD anlegen und einer VM zur Verfügung stellen](/dingx/server/Hyper-V/ps_newVHD.md)
* [Erweitern der Speicherkapazität von Ubuntu](/dingx/server/Hyper-V/vm_lvm_expand.md)
