##Ein Backup für eine VM einrichten
Erstellt von Hauke Czora am 13.10.2017
###Eine weitere VM zusätzlich vom Hypervisor sichern
1. Auf dem Backupserver die Verzeichnisse für die Sicherungen anlegen:
  * in /backup/backup/
  * in /backup/lts/
  * in /home/ftpuser/ftproot/
  * die drei Verzeichnisse müssen den gleichen, einzigartigen Namen haben, dieser ist im Powershellskript auf dem Hypervisor als 'Hostname' anzugeben
1. Auf dem Hypervisor die backup.bat erweitern:
  * mit ´notepad C:\Skripte\backup.bat´ die Datei öffnen
  * vor dem ´exit´ Befehl einen weiteren Aufruf des Powershellskriptes einfügen (copy&paste) und die Parameter ändern
    * -hostname auf den Verzeichnissnamen setzen
	* -vmname auf den Namen der VM setzen
###Auf einem neuen Hypervisor die Sicherung einrichten
1. Auf dem Backupserver die Verzeichnisse für die Sicherungen anlegen:
  * in /backup/backup/
  * in /backup/lts/
  * in /home/ftpuser/ftproot/
  * die drei Verzeichnisse müssen den gleichen, einzigartigen Namen haben, dieser ist im Powershellskript auf dem Hypervisor als 'Hostname' anzugeben
1. Auf dem Hypervisor die backup.bat erstellen:
  * mit ´notepad C:\Skripte\backup.bat´ die Datei öffnen
  * ´powershell -command "C:\Scripts\BackupPush.ps1 -vmname \"vmname\" -hostname \"verzeichnisname\" -ftpaddress \"192.168.222.62\"; exit $LASTEXITCODE"´
    * -hostname auf den Verzeichnissnamen setzen
	* -vmname auf den Namen der VM setzen
  * ´echo %errorlevel%´
1. 7za.exe und pscp.exe in 'C:\Program Files' ablegen, BackupPush.ps1 in 'C:\Scripts' ablegen
1. Mit pscp.exe auf den Backupserver zugreifen (manuell!), um das Zertifikat zu speichern (sonst funktioniert die automatische Sicherung nicht):
  * ´"C:\Program Files\pscp.exe" -v -pw ftpuser -ls ftpuser@192.168.222.62:/ftproot/´
1. Einen Task für das Ausführen der Batchdatei anlegen:
  * ´schtasks /create /SC HOUR /MO 6 /TN myTasks\Backup /TR "'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe' -ExecutionPolicy Bypass -File C:\Scripts\BackupPush.ps1" /RL HÖCHSTE´
