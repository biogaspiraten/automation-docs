#Eine neue VHD anlegen und einer VM zur Verfügung stellen
Erstellt von Hauke Czora am 10.07.2018
* auf dem Hypervisor Powershell öffnen
* ´New-VHD -Path 'C:\users\Public\Documents\Hyper-V\Virtual hard disks\<DiskName>.vhdx' -SizeBytes <SizeInGB>GB´
  * <DiskName> ist der Dateiname der neuen virtuellen Festplatte
  * <SizeInGB> ist die maximale Kapazität in GigaByte
* ´Add-VMHardDiskDrive -VMName <VMName> -Path 'C:\Users\Public\Documents\Hyper-V\Virtual hard disks\<DiskName>.vhdx'´
  * <VMName> ist der Name der VM, die existierenden VMs können mit ´Get-VM´ aufgelistet werden
  * <DiskName> ist der Dateiname der zuvor erstellten virtuellen Festplatte