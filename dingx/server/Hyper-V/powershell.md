#Hyper-V Remoteadministration mit RDP und Powershell
Erstellt von Hauke Czora am 26.04.2018
##RDP-Verbindung starten
* Windows-Taste drücken
* 'rdp' eingeben und mir Enter bestätigen
* die IP-Addresse oder den Rechenernamen eingeben
* im Feld Benutzername den Rechnernamen/die IP-Addresse und den Benutzernamen getrennt durch '\' eingeben
* im Feld Passwort das Passwort eingeben
* geggebenenfalls das Zertifikat akzeptieren
##Powershell starten
* in der Kopmandozeile 'powershell' eingeben und Enter
##Übersicht über vorhandene VMs
 Get-VM 
##VM starten und stoppen
 Start-VM [VMName]
 Stop-VM [VMName]

