##Beispiel reciver.json
Erstellt von Hauke Czora am 08.12.2017

Modifizierte Version des DingX-Connector Empfänger; Die Anpassungen umfassen zwei Variablen für den Status und den Zeitpunkt der letzten empfangenen Nachicht (als Integer), eine zweite Influx-Instanz, Statusübernahme bzw. Aktualisierung des Zeitpunktes der letzten empfangenen Nachicht, sowie eine Prüfschleife, die den Timeout überprüft und den Status in die Datenbank schreibt.

```javascript
	var mqtt = require('mqtt');
	var influx = require('influx');
	var async = require('async')
	var config = require('./config.json');
	var lastmsg = 0;
	var state = -6;//preinit
	
	/*--- I N F L U X ---*/
	var influxClient = new influx.InfluxDB({
	    host: 'localhost',
	    port: 8086, // optional, default 8086
	    protocol: 'http', // optional, default 'http'
	    username: config.influx.username,
	    password: config.influx.password,
	    database: config.influx.database
	});
	
	var influxMonit = new influx.InfluxDB({
	    host: 'localhost',
	    port: 8086, // optional, default 8086
	    protocol: 'http', // optional, default 'http'
	    username: config.monitoring.username,
	    password: config.monitoring.password,
	    database: config.monitoring.database
	});
	
	/*--- M Q T T ---*/
	
	var mqttClient = mqtt.connect(config.mqttbroker, config.mqtt);
	
	mqttClient.on('connect', function () {
	    console.log('MQTT online');
	    mqttClient.subscribe(config.topic);
		state = 0;
	});
	
	mqttClient.on('error', function () {
	    console.log('MQTT Fehler');
		state = -4;
	});
	
	mqttClient.on('offline', function () {
	    console.log('MQTT offline');
		state = -2;
	});
	
	mqttClient.on('reconnect', function () {
	    console.log('MQTT reconnect');
	});
	
	mqttClient.on('message', function (topic, message) {
	    //Daten vom ntonline_Connector_S7_V2
	    //console.log(JSON.parse(message));
	    influxClient.writePoints(JSON.parse(message));
		lastmsg = Date.now();
	});
	
	/*--- M O N I T O R ---*/
	setInterval(function () {
		var monit;
		if (Date.now() - lastmsg >= config.monitoring.msgtimeout) {
			monit = 0;
		}
		else {
			monit = 1;
		}
		monit = monit + state;
		//console.log([{ measurement: 'dingx_connector_monitor', tags: { connector: config.influx.database }, fields: { sender_OK: monit }}]);
		influxMonit.writePoints([{ measurement: 'dingx_connector_monitor', tags: { connector: config.influx.database }, fields: { sender_OK: monit }}]);
	}, config.monitoring.interval);
```
