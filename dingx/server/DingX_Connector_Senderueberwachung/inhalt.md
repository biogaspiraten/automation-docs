#Dokumentation der DingX-Connector Senderüberwachung
Erstellt von Hauke Czora am 08.12.2017
##Inhalt
* [Übersicht](/dingx/server/DingX_Connector_Senderueberwachung/doku.md)
* [Beispiel config.json](/dingx/server/DingX_Connector_Senderueberwachung/config.md)
* [Beispiel reciver.js](/dingx/server/DingX_Connector_Senderueberwachung/reciver.md)