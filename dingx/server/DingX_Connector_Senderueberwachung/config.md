##Beispiel config.json
Erstellt von Hauke Czora am 08.12.2017

Anstelle der IP-Adresse oder des DNS-Eintrags der AWS-Instanz des MQTT-Brokers, wird die Adresse mqtt.north-tec.net verwendet, die bei Strato auf die IP der Instanz zeigt. Dadurch muss bei Änderung der IP nur der DNS-Eintrag bei Strato verändert werden.

```json
	{
	    "mqttbroker": "tcp://mqtt.north-tec.net",
	    "mqtt": {
	        "username": "northtec",
	        "password": "humpiedumpie",
	        "port": 1883,
	        "clientId": "ntonline-server-clientid",
	        "keepalive": 10,
	        "connectTimeout": 30000,
	        "clean": false,
	        "reconnectPeriod": 10000,
	        "protocollId": "MQTT",
	        "protocolVersion":4
	    },
	    "topic": {"topic/#":1},
	 
	    "influx": { 
	        "database": "databasename"
	    },
	
	    "monitoring": {
	        "interval": 6000,
	        "msgtimeout": 30000,
	        "database": "connector_monitoring",
	        "username": "north-tec",
	        "password": "humpiedumpie"
		}
	}
```