##Übersicht über die DingX-Connector Senderüberwachung
Erstellt von Hauke Czora am 08.12.2017

|Ausgabewert|Bedeutung|
|---|---|
|-6|Fehler bei der Prozessinitialisierung ohne Nachichtenempfang|
|-5|Fehler bei der Prozessinitialisierung mit Nachichtenempfang|
|-4|MQTT-Fehler (error Event ausgelößt) ohne Nachichtenempfang|
|-3|MQTT-Fehler (error Event ausgelößt) mit Nachichtenempfang|
|-2|MQTT-Broker offline oder keine Zugangsberechtigung ohne Nachichtenempfang|
|-1|MQTT-Broker offline oder keine Zugangsberechtigung mit Nachichtenempfang|
|0|Verbunden mit MQTT-Broker kein Nachichtenempfang|
|1|Verbunden mit MQTT-Broker und Nachichtenempfang|
|n/a|Empfängerdienst ausgeschaltet oder Problem mit der Datenbankverbindung|

In Grafana kann es auf Grund von Rundungsfehlern zu Ungenauigkeiten kommen, für eine Textausgabe als Singlestat sollte daher einerseits ein relativ kurzes Timerange (ggf. per Override) gewählt werden und andererseits ein 'range to text' Valuemapping angewendet werden. Die Daten kommen direkt aus der Empfangseinheit des jeweiligen DingX-Connector PM2-Service, der Überwachungsintervall (default 6 sec) und der Timeout (die Zeit nach der von 1 auf 0 geschaltet wird, wenn keine Nachicht empfangen wird; default 60 sec) können genau wie die Zieldatenbank über die Konfigurationsdatei angepasst werden. Eine Anpassung des Bezeichner-Tags ist nicht vorgesehen, es wird die Bezeichnung der Ausgabedatenbank verwendet.