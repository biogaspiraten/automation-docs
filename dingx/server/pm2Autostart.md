##pm2 automatischer Prozessstart beim Systemstart
Erstellt von Hauke Czora am 17.10.2017
###Ersteinrichtung
* pm2 installieren (als sudo)
* alle Prozesse starten (als sudo)
* `sudo pm2 startup` Erzeugt die Startup-Datei für pm2
* `sudo pm2 save` Speichert die Prozessliste, sodass alle Prozesse beim Neustart geladen werden
###Bei Hinzufügen von neuen Prozessen
* neuen Prozess / neue Prozesse starten
* `sudo pm2 save` Speichert die Prozessliste, sodass alle Prozesse beim Neustart geladen werden