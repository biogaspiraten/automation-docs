Betriebsmittel-Erweiterung

Anforderung
- Session
- Benutzerberechtigung
- 

```json
{ 
    "_id" : {BSON:ObjectId}, 
    "_parent_id" : {BSON:ObjectId}, 
    "Bezeichnung" : {string}, 
    "Ort" : {
        "_id" : "577d28515adc64bc5ea5dff8", 
        "Bezeichnung" : "Lager Manningen"
    }, 
    "Kunde" : {
        "_id" : ObjectId("56726c41194d03c418a3550e"), 
        "Bezeichnung" : "SANI GmbH"
    }, 
    "Eigenschaften" : {
        "allgemein" : {
            "property" : "value", 
            ...
            ...
            ...
        },
        "physikalisch": {
            "property" : "value", 
            ...
            ...
            ...
        },
        "wirtschaftlich": {
            "property" : "value", 
            ...
            ...
            ...
        }
    }, 
    "Template" : {
        "_id" : "58f71c3be5ae4b19d8795b9c"
    }, 
    "access_rights": {        
        "owner": {BSON:ObjectId}, //Benutzer ObjectId
        "owner_mode": {string}, //read, write, full
        "groups": [{_id:ObjectId,role:{string}], //Benutzergruppen aus der Customer-Collection
        "users": [{_id:ObjectId,role:{string}]
    },
    "service" : {object}, 
    "geotracking" : {object} 
}

```


####Zugriffsrechte auf dem Betriebsmittel
```json
"access_rights": {        
        "owner": {BSON:ObjectId}, //Benutzer ObjectId        
        "groups": {BSON:ObjectId}, //Benutzergruppen aus der Customer-Collection
        "users": [BSON:ObjectId]
    },
```

Anforderungen
- owner dürfen alles
- Besitzrechte können übertragen werden
- wenn *access-right* nicht vorhanden = Vollzugriff auf das Betriebsmittel
- wenn *access-right* fehlerhaft = Vollzugriff auf das Betriebsmittel


###Customer-Collection
```json
{ 
    "_id" : ObjectId("5a1eb5173e143f068f939d0d"), 
    "Kundenname" : {string}, 
    "Strasse" : {string}, 
    "Ort" : {string}, 
    "Postleitzahl" : {string}", 
    "Nummer" : {string}, 
    "logo" : "ht_logo.svg", 
    "style" : "hellermanntyton",     
    "settings": [
        { 
          "type": "access-control",
          "groups": [{
                "name": {string},
                "default-role" {string}, //full, writer, reader 
                "members":[{BSON:ObjectId}]
          }]  
        },
        {
        "type":"business",
        "asset_charge": {
            "collective_groups": [
                {
                    "group": {number},
                    "price": {float},
                    "currency": {string} //ISO Code

                }
            ]
        }]
    }
    "_config" : {
        "menu-items" : []
    },

}
```

###Erweiterung der Customer-Collection
```json
"business_settings":
        "asset_charge": {
            "collective_groups": [
                {
                    "group": {number},
                    "price": {float},
                    "currency": {string} //ISO Code
                }
            ]
        }
```
