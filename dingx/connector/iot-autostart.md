# Linux Yocto
## Autostart

# Schritt 1. : Bash script
### Pfad wechseln
 + Kommandozeile:
        ```
        cd /etc/init.d
        ```

### Erstellen und editieren
+ Kommandozeile: 
        ```
        nano SERVICENAME.sh
        ```
+ ##### Damit das Skript interpretiert werden kann muss in der ersten Zeile der         Compiler eingetragen werden
        #!/bin/bash -e
        
### Ausführenden Befehl hinzufügen
        Kann wie ein Konsolenbefehl eingegeben werden (alles in einer Zeile)
        (Es muss der absolute Pfad zur Datei angegeben werden, z.B /usr/local/bin/autossh)


### Fehlerausgabe (optional):
Um zu Debuggen, falls das Programm nicht gestartet wird, kann man folgendes an den Anfang der Datei hinzufügen:

```
            exec 3>&1 4>&2
            trap 'exec 2>&4 1>&3' 0 1 2 3
            exec 1> DATEINAME.log 2>&1
```
### Speichern
+ Kommandozeile:
        ```
        Strg + x -> Y -> Enter
        ``` 
#Schritt 2. : Skriptrechte verändern
### Rechte verändern
+ Kommandozeile
        ```
        chmod +x SERVICENAME.sh
        ```

#Schritt 3. : Im Runlevel integrieren
  ### Skript zum Runlevel hinzufügen
+ Kommandozeile:        
        ```
        update-rc.d SERVICENAME defaults 80 10
        ```

* ### Erklärung:
        -> defaults: Fügt zu den gewöhnlichen Runleveln hinzu (Es gibt 6, eins für jede Aktion des Systems)
        -> 80: Priorität des Dienstes - start (von 0 - 99, 80 wird spät gestartet)
        -> 10: Priorität des Dienstes - stop (von 0-99, 10 wird früh beendet)


#Schritt 4. : System neustarten
### Neustart
+ Kommandozeile:
        ```
        reboot
        ```

* ### Nach Neustart Dienste überprüfen
```
    top 
    Schift + M
    Mit Pfeiltasten nach oben und unten navigieren
    Schließen mit Strg + C
```