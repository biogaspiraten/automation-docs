# Server: IoT Verbindung einrichten

Um auf dem Server die Verbindung des IoT entgegenzunehmen muss ein Benutzer eingerichtet werden, welcher auch auf dem IoT in der Datei "service_tunnel.sh" benutzt wurde.

1. ### Benutzer anlegen
+ Kommandozeile:
    ```
    sudo adduser USERNAME
    ```
+ Passwort für den Benutzer eingeben (Passwort gleich Benutzer)
    (Server: Passwortanmeldung blockiert, kein Problem)
+ Restliche Abfragen mit Enter beantworten

2. ### User wechseln
+  Kommandozeile:
   ```
   su USERNAME
   ```

3. ### SSHKey generieren
+ Kommandozeile:
  ```
  ssh-keygen
  ```
+ alles mit Enter beantworten

4. ### Dateiberechtigung wechseln
+ Kommandozeile:
    ```
    chmod 400 ~/.ssh/id_rsa
    ```

5. ### Key authorisieren
+ Kommandozeile:
    ```
    cat ~/.ssh/id_rsa >> authorized_keys
    ```

6. ### Ordnerberechtigung wechseln
+ Kommandozeile
    ```
    chmod 600 ~/.ssh
    ````

7. ### Key kopieren
+ Kommandozeile:
    ```
    cat ~/.ssh/id_rsa
    ```
+ Key kopieren, gleiches Format nötig (schmaler Block, mehrere Zeilen)
  
8. ### Key auf IoT einfügen
+ Auf dem IoT einloggen
    + Kommandozeile:
        ```
        nano ~/.ssh/service_key
        ```
        + Key einfügen
            + Abspeichern

9. ### Autostart auf dem IoT einrichten
+ Wird in der Anleitung "iot-autosshkonfiguration.md" erklärt
