# Einrichtung von Autossh auf dem IoT

* ### Name des Skripts
    ```
    service_tunnel.sh
    ```



* ### Speicherort des Skripts
    ```
    /etc/init.d/service_tunnel.sh
    ```

### Konfiguration des Skripts verändern
+ #### Der Port und der Login des Users muss verändert werden
    Der Port fing bei 10000 an und wird ab dann weiter hochgezählt. Um zu wissen welcher Port als nächstes dran ist auf dem Server nachschauen.
    Wie man diese sieht wird in der Anleitung "Iot-ssh-via-server.md" beschrieben.
    (Schritt 2. "| ohne grep")

    ```
    ANDERER_PORT:127.0.0.1:22
    ANDERER_USER@service.dingx.net
    ```
    Anschließend Datei mit Strg + x,dann y dann Enter abspeichern

    Wie man die Datei "service_tunnel.sh" zum Autostrart hinzufügt wird in der Anleitung "iot-autostart.md" erklärt
