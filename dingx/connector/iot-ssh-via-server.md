# Verbindung zu einem IoT über der Server herstellen

1. Momentan ist der Server cctv.dingx.net (läuft auch über die Adresse service.dingx.net)

## Vorraussetzungen:
* IoT ist mit einem SSH-Tunnel am Server angemeldet
* IoT Passwort
* Server zugriff

1. ## Mit dem Server verbinden
* via Putty mit dem Serverkey (Passwort ist gesperrt)

2. ## Anliegende Verbindung überprüfen
```
sudo lsof -i -N -P | grep KUNDENNAME
```

3. ## Port raussuchen
* Wenn der Eintrag angezeigt wurde ist das IoT verbunden
* Auf der rechten Seite sollte etwas in der Syntax stehen:
   localhost:DER_PORT (LISTEN)

4. ## Mit dem IoT verbinden
* Nun über die Kommandozeile mit dem IoT verbinden
```
ssh root@localhost -p DER_PORT
```

### Erklärung
root: Benutzername des angesprochenen Geräts

Nach der Passworteingabe ist man mit dem Gerät verbunden.
Man kann es mit dem Befehl "exit" verlassen und auf die Server-Kommandozeile zurückgehen