# Dingx-connector einrichten: Teil 1 - Den Sender installieren #

6.Oktober 2017 - Björn Riekenberg

Der dingx-connector besteht aus den Komponenten **sender** und **receiver** die mit node.js erstellt wurden. Der **sender** wird an der Maschine/Anlage installiert, der **receiver** auf dem Server. Dieses Dokument behandelt die Installation des **sender**.

Als erstes muss die aktuelle stabile Version von [node.js](https://nodejs.org/en/) installiert werden.

Dann läd man den aktuellen Quellcode aus dem [repository](https://bitbucket.org/biogaspiraten/dingx-connector) von Bitbucket in ein neues Verzeichnis und führt eine Installation durch

```shell
npm install
```

## Installation unter Linux ##

Für die Installation unter Linux sind keine weiteren Schritt notwendig.

## Installation unter Windows ##

Bei der Installation des Senders unter Windows müssen als erstes noch Abhängigkeiten und Compiler installiert werden. Das geht am einfachsten mit folgenden Befehl:

```shell
npm install --global --production windows-build-tools
```

Wird dieser Schritt nicht durchgeführt, schlägt die Kompilierung von gyp-Modulen fehl. Erst danach kann die Installation der Anhängigkeiten durchgeführt werden:

```shell
npm install
```

## Installation auf dem IoT2040 ##

Am einfachsten ist es ein vorbereitets Image (externe Festplatte) per Win32DiskImager auf eine 16 GB SD-Karte von SanDisk kopieren und anschließend per sftp (port 22) nur die geänderten config.json und csv-Datein auf das Gerät zu übertragen. Auf dem zweiten Ethernet Port bezieht das Gerät eine IP-Adresse über DHCP.

Die Zugangsdaten für das aktuelle Dingx-Image lauten:

```shell
User: root
Passwort: nt#007
```

Das Standard Image von Siemens wird per Win32DiskImager auf eine 16 GB SD-Karte von SanDisk kopiert. Anschließend verbindet man den Ethernet Port 2 mit einem DHCP-Server und kann sich anschließend per Putty oder sftp (Port 22) verbinden, um das Projekt auf das IOT2040 zu übertragen.

Anschließend führt man die Installation aus und installiert  pm2 auf dem Gerät:

```shell
npm install
npm install  pm2 -g
```

Dann wir der Sender mit folgenden Befehlen gestartet und der autostart bei pm2 aktiviert:

```shell
pm2 start sender
pm2 save
pm2 startup
```

Wenn mit dem IoT2040 Daten über die seriellen Schnittstellen ttyS2 (X30) oder ttyS3 (X31) übertragen werden sollen, muss das modul 'serialport' komplett übersetzt werden, sonst bricht der dingx-connector mit der Fehlermeldung 'Illigal Instruction' ab. Dieser Fehler tritt bei der Verwendung von 'frischen' Images auf. Bei den vorkonfigurierten dingx-Images ist dieser Fehler bereits behoben.

```shell
npm rebuild serialport --build-from-source
```

## Receiver ##

Der Receiver empfängt die Daten des Senders und schreibt sie in InfluxDB. Es wird pro Kunde eine Instanz vom Receiver auf dem Server (192.168.222.25) ausgeführt. Für einen neuen Kunden verbindet man sich per ftp mit dem Server und erstellt ein neues Verzeichnis:

```shell
sudo mkdir dnx-kundeX

```

Anschließend kopiert man die Dateien des Receivers in dieses Verzeichnis und führt eine Installation durch.

```shell
npm install
sudo pm2 start dnx-kundeX.js
sudo pm2 save

```

 Die Datei config.json muss vorher so angepasst werden, dass das topic und die Datenbank zum Kunden passen. Datei receiver.js wird für die Instanz umbenannt in dnx-kundeX.js. Außerdem muss in influx eine neue Datenbank für den Kunden angelegt werden.

```shell
influx
> CREATE DATABASE kundeX
```
