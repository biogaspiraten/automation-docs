# Anbindung einer Raspberry-Kamera

## Einrichten des MJPEG-Streamers:
Via SSH als User `pi` auf dem Raspberry anmelden.

```
cd ~
sudo apt-get install cmake libjpeg-dev
mkdir src
cd src
git clone https://github.com/jacksonliam/mjpg-streamer
cd mjpg-streamer/mjpg-streamer-experimental
make

```

Testen via:

```
sudo ./mjpg_streamer -o "output_http.so" -i "input_raspicam.so -x 1280 -y 1024"
```

Der Stream kann dann unter der URL

```
http://127.0.0.1:8080/?action=stream
```

vom Raspberry angesehen werden, bzw. auch von jedem anderen Rechner wenn die IP-Adresse `127.0.0.1` durch die IP des Raspberry ersetzt wird.

Wenn dies funktioniert in an `/etc/rc.local` vor dem `exit 0`-Statement folgendes einfügen:

```
export LD_LIBRARY_PATH="/home/pi/src/mjpg-streamer/mjpg-streamer-experimental"
/home/pi/src/mjpg-streamer/mjpg-streamer-experimental/mjpg_streamer -b -o "output_http.so" -i "input_raspicam.so -x 1280 -y 1024 -fps 1"
```


## Einrichten der ersten Kamera auf diesem IoT-Device
Falls in `/etc/rc.local` noch keine SSH-Weiterleitung zum AWS-Relay existiert, muss diese Anweisung vor dem `exit 0`-Statement eingefügt werden:

```
/usr/bin/autossh -f -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -i /root/.ssh/id_northtec_testkunde_01 testkunde01@ec2-54-77-80-80.eu-west-1.compute.amazonaws.com -R 9001:127.0.0.1:8080 -N
```

Dabei sind der SSH-Key von `testkunde_01`, der über den `-i`-Parameter angesprochen wird, sowie der Loginname `testkunde01@ec2…` entsprechend dem aktuellen Kunden anzupassen. Der Port `8080` muß bei Bedarf auf die Portadresse des eben eingerichteten MJPEG-Streams abgeändert werden.

Damit ist die IoT-seitige Einrichtung der Kamera abgeschlossen.

## Einrichten parallel zu bereits existierender Kamera auf diesem IoT-Device
Falls in `/etc/rc.local` bereits eine Kameraweiterleitung zum AWS-Relay existiert wird lediglich ein neuer `-R`-Parameter hinzugefügt. Beispiel:

Zusätzlich zum bereits bestehenden

```
-R 9001:127.0.0.1:8080
```

– welches nicht abgeändert werden darf – wird ein weiterer Parameter dieser Form angehängt.  

```
-R 9001:127.0.0.1:8080 -R 9002:127.0.0.1:8081
```

Der Port `8081` muß auf die Portadresse des eben eingerichteten MJPEG-Streams abgeändert werden.

Nach dem Abspeichern das Skripts sollte der Raspberry neu gestartet werden. Dadurch wird zum einen der neue Tunnel aktiv, zum anderen kann nachvollzogen werden ob das Skript nach eine Neustart korrekt aktiv wird.

 
## MotionEye Einrichtung
In das Verzeichnis "motioneye" wechseln (/home/pi/motioneye) und den systemintegrierten Check ausführen. 

```
sudo python setup.py check
```


Anschließend werden zwei Ordner erstellt, um die Config- und Fotodaten zu verwalten:

```
sudo mkdir -p /etc/motioneye && sudo mkdir -p /var/lib/motioneye
```


## MotionEye Autostart
Nachdem die Überprüfung erfolgreich war muss MotionEye in den Autostart aufgenommen werden durch:

```
sudo mv /home/pi/motioneye/extra/motioneye.conf.sample /etc/motioneye/motioneye.conf
sudo cp /home/pi/motioneye/extra/motioneye-systemd-unit-local /etc/systemd/system/motioneye.service
sudo systemctl daemon-reload
sudo systemctl enable motioneye
sudo systemctl start motioneye
```


## MotionEye Auto-SSH
Damit eine automatische Verbindung hergestellt wird muss die `tunnel.sh` Datei konfiguriert werden.

- `sudo nano /home/pi/tunnel.sh`
- `testkundeXX`@ec2... Testkunde durch aktuellen ersetzen 
- `9004:127.0.0.1:8081` 9004 durch aktuellen AWS-Port ersetzen (Port zum Stream).
- `9008:127.0.0.1:8765` 9008 durch aktuellen AWS-Port ersetzen (Port zur Homepage).

## AWS-Firewall für 

## AWS-Firewall für MotionEye Homepage
Da der Port 8765 bzw. 9008 ohne Weiterleitung, sondern direkt über den AWS-Server via HTTP ansprechbar sein soll, muss auf diesem noch eine entsprechende Portweiterleitung eingerichtet werden:

```
iptables -t nat -A PREROUTING -p tcp --dport 9008 -j DNAT --to-destination 127.0.0.1:9008
```

Mit diesem Befehl ist der Localhost-Port 9008, der via Tunnel auf Port 8765 des Raspberrys weitergeleitet wird, über Port 9008 aus dem Internet zu erreichen.


## MotionEye Hostnamen
Nun muss der Host angepasst werden. Je nach dem wie viele Kameras angebunden sind, werden die Nummern XX verteilt.

- `sudo nano /etc/hosts`
- Unten `127.0.0.1 XXXXXXXXXXX` in `North-Tec-Cam-XX`verändern.
- Durch `Strg+x,y und Enter` wird der Eintrag gespeichert


Als nächstes wird der Hostname definiert:

- `sudo nano /etc/hostname`.
- durch `North-Tec-Cam-XX` ersetzen.

## MotionEye Homepage (Zugang)
`69bf609d3a3200bf619c4c15dd21743ad6f7a631` entspricht `northtec2017`

- `sudo nano /etc/motioneye/motion.conf`
- `@admin_password` mit obiger Zeichenkette ergänzen.
- `@normal_password` mit `northtec2017` ergänzen.
- Durch `Strg+x,y und Enter` wird der Eintrag gespeichert.

## MotionEye Kamerakonfiguration
Auf der Adresse `localhost:8765` oder `http://ec2-54-77-80-80.eu-west-1.compute.amazonaws.com:[PORT HOMEPAGE AWS]/` kann man sich nun mit `admin` und `northtec2017` anmelden.
