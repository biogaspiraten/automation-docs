# Setup Neukunde für dingx-Kameraanbindung


## Überblick
Bevor eine Kamera für einen Kunden angebunden werden kann, muss dieser einmal auf dem Relay-Server eingerichtet werden. Danach können für einen einmal aufgesetzten Kunden im Prinzip beliebig viele Kameras eingerichtet werden.


## Kundenspezifischen SSH-Key erstellen

Jeder Kunde erhält seinen eigenen SSH-Key um sich auf dem Kamera-Relay anzumelden. Zum Erstellen eines Keys kann man sich z.B. auf dem AWS-Server anmelden und im Falle eines Kunden mit dem Namen „Testkunde_01“ folgendes Kommando ausführen:  
`ssh-keygen -o -a 100 -t ed25519 -f id_testkunde_01 -N ''`

Dadurch werden die folgenden Files erstellt:

- Der Private Key in `id_northtec_testkunde_01`.
- Der Public Key in `id_northtec_testkunde_01.pub`.

Diese werden in ein `testkunde_01-keys.zip`-File archiviert und in der [Download-Area des „camera-mjpeg-relay“-Projekts](https://bitbucket.org/biogaspiraten/camera-mjpeg-relay/downloads/) gespeichert.


## Kundenspezifisches Zugangstoken erstellen

Für jeden Kunden wird ein Zugangstoken, praktisch ein langes Passwort, erstellt, mit dem sichergestellt wird dass nur berechtigte Personen auf das Kamera-Relay zugreifen. Jeder beliebige Textstring kann dabei als Token dienen, da er jedoch nicht eingegeben werden muss sondern nur maschinell verwaltet wird sollte es eine längere, möglichst eindeutige Zeichenfolge sein. Als Token kann z.B. auf der Linux-Kommandozeile via

```
uuidgen
```

ein Token der Art `CFAA906C-E516-4763-B543-B2E287F43ADB` erzeugt werden. Dieses wird zum Archiv `testkunde_01-keys.zip` in der [Download-Area des „camera-mjpeg-relay“-Projekts](https://bitbucket.org/biogaspiraten/camera-mjpeg-relay/downloads/) hinzugefügt.


## Useraccount auf AWS-System anlegen

Für jeden User muss zwangsweise ein eigener Useraccount auf dem AWS-System angelegt werden. Dies ist notwendig um die SSH-Restrictions, die user-basiert
arbeiten, anlegen zu können.

Beispiel:

```
useradd -m testkunde01
su - testkunde01
mkdir .ssh
chmod 700 .ssh
cd .ssh
```


Danach `cat >> authorized_keys` ausführen und kundenspezifischen Public-Key via Copy/Paste einfüge, danach mit CTRL-D abschließen oder alternativ `.ssh/authorized_keys` entsprechend mit einem Editor bearbeiten.

Jetzt sollte `ssh -i .ssh/id_northtec_testkunde_01 testkunde01@ec2-54-77-80-80.eu-west-1.compute.amazonaws.com`
vom IoT aus funktionieren.

Um zu verhindern dass man vom IoT-Device aus eine Shell auf dem Relay-Server starten kann muss die `/etc/ssh/sshd_config` folgendermaßen angepasst werden:

```
X11Forwarding no
Match User testkunde01
  AllowAgentForwarding no
  ForceCommand echo 'Shell access not allowed.'
```

Bei einem erneuten Ausführen des oben angegebenen ssh-Testbefehls muss jetzt er Hinweis erscheinen dass es nicht gestattet ist eine Shell auf dem Server zu starten.
