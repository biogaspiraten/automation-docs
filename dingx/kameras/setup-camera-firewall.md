# Setuanweisungen für alle Kameras

## Firewall
Per Default sind mit Ausnahme von SSH keine Ports von außen zugänglich, da alle von einer der Instanz unabhängigen Firewall („Amazon-Firewall“) blockiert werden. Die Ports müssen über die separat über AWS-Konsole verwaltet werden.

Die Firewall wird über sog. „Security Groups“ gesteuert. Jede Security Group kann eine oder mehrere Regeln umfassen. Im Rahmen der Northtec-Kamerarelays wird für jeden Kunden eine eigene Security Group angelegt. Dies macht zum einen das Management übersichtlicher und erlaubt es außerdem die Firewall kundenspezifisch zu verwalten.

Security Groups werden im EC2-Dashboard verwaltet. In der Übersicht findet sich im Abschnitt „Network & Security“ der Menüpunkt „Security Groups“. Hier können neue Security Groups hinzugefügt und bestehende verwaltet werden.

Für jeden Kunden wird eine eigene Security Group mit dem Namen „camrelay-`<kundenname>`“ angelegt. Relevant sind für uns die „inbound rules“. Um einen Port freizuschalten müssen bei den Inbound Rules die Felder wie folgt ausgefüllt werden:

- Type: `Custom TCP Rule`
- Protocol: `TCP`
- Port Range: (Start- bis Endport durch „-“ getrennt)
- Source: `Custom` / `0.0.0.0/0`



