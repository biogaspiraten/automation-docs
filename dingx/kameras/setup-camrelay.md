# Übersicht zur Anbindung von Kameras an Dingx




## Folgende Anleitungen sind in diesem Kontext vorhanden:

- [setup-camrelay](setup-camrelay)  
Diese allgemeine Einführung.    

- [setup-camrelay-awsinstance](setup-camrelay-awsinstance)  
Beschreibt das Aufsetzen einer AWS-Instanz als Kamera-Relay.


### Schritte für Neukunden-Setup

- [setup-camrelay-neukunde](setup-camrelay-neukunde)  
Beschreibt das Einrichten eines neuen Kunden für das Relay-System.


### Einrichten von IoT-Systemen

- [setup-camrelay-iot-simatic](setup-camrelay-iot-simatic)  
Enthält die Einrichtungsanweisungen für ein Simatic IOT 2000 als Anbindungspunkt für Kameras an Dingx.

- [setup-camrelay-iot-raspberry](setup-camrelay-iot-raspberry)  
(ToDo)

### Anbindung von Kameras an IoT-Systeme

- [setup-camrelay-camera-burgcam-bullet304](setup-camrelay-camera-burgcam-bullet304)  
Anbindung einer BurgWächter BulletCal 304 an ein Kunden-IoT-System.

- [setup-camrelay-camera-mobotix-d24](setup-camrelay-camera-mobotix-d24)  
Anbindung einer Mobotix D24 an ein Kunden-IoT-System.

- [setup-camrelay-camera-raspberry](setup-camrelay-camera-raspberry)  
Anbindung einer Raspberry-Kamera an ein Raspberry-IoT-System.


### Allgemein

#### Wartungstunnel benutzen:
```
ssh pi@localhost -p 9006
(Passwort?)
```


