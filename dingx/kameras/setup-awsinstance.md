
# Setup Kamerarelay

## Einsatz einer Amazon EC2-Instanz als Kamerarelay
- Bei `aws.amazon.de` mit `briekenberg@north-tec.de` anmelden.
- Zu EC2-Service.
- Das Kamerarelay ist vom Typ `t2.micro`.
- Die bisherige Instanz lief in der Zone `eu-west-1a`.
- Operating System: Ubuntu.
- Key: `aws_mqtt_key`

### AWS SSH-Login

### Setup
In `/etc/apt/sources.list.d`:

```
deb https://deb.nodesource.com/node_8.x stretch main
deb-src https://deb.nodesource.com/node_8.x stretch main
```


```
apt-get update
apt-get -V dist-upgrade
apt-get install nodejs
npm install pm2

cd /var/www
git clone https://bitbucket.org/biogaspiraten/camera-mjpeg-relay
```

Aktuell enthält dieses Repository ein Skript `run-pm2.sh` mit dem alle Node-Instanzen über den pm2-Prozessmanager ausgeführt werden können.

*ToDo:*  
Generelle Anweisungen zum pm2-Management umsetzen: Autostart, etc.


### Aktuelle Instanz
Hostname: `ec2-54-77-80-80.eu-west-1.compute.amazonaws.com`.
