# Übersicht zur Anbindung von Kameras an Dingx

Diese Anleitung ist in eine Liste von Anwendungsfällen – Neukunde einrichten, Kamera zu bestehendem Setup hinzufügen – aufgeteilt, die im folgenden beschrieben werden.



## Relay-/AWS-Setup
Zur Anbindung der Kameras ist aus techinschen Gründen ein Weiterleitungsserver erforderlich. Dieser wird aktuell über eine AWS-Instanz realisiert. Ohne einen Relay-Server ist keine Kameraanbindung möglich. Die einzelnen Schritte sind hier beschrieben:  

- [Setup-AWSInstance](./setup-awsinstance.md)


### Schritte für Neukunden-Setup
Um Kameras für einen Kunden an einen bestehenden Relayserver anbinden zu können, muss der entsprechende Kunde einmal initial eingerichtet werden:  

- [Setup-Neukunde](./setup-neukunde.md)


### Einrichten von IoT-Systemen
Um für einen bereits eingerichteten Kunden eine Kamera hinzufügen zu können, wird auf Kundenseite ein IoT-Device benötigt, welches die entsprechende Verbindung zum Relay-Server verwaltet. Je nach IoT-Device sind folgende Schritte erforderlich:

- [Setup-IoT-Simatic](./setup-iot-simatic.md)  
- [Setup-IoT-Raspberry](./setup-iot-raspberry.md)


### Anbindung von Kameras an IoT-Systeme
Um eine Kamera an ein bestehendes IoT-Device anzubinden muss je nach Kamera eine der folgenden Anleitungen ausgeführt werden:

- [Setup-Camera-BurgCam-Bullet304](./setup-camera-burgcam-bullet304.md)
- [Setup-Camera-Mobotix-d24](./setup-camera-mobotix-d24.md)
- [Setup-Camera-Raspberry](./setup-camera-raspberry.md)  

Außerdem muss für jeden Kameraport explizit ein Port auf der AWS-Firewall freigegeben werden:

- [Setup-Camera-Firewall](./setup-camera-firewall.md)

