# Setup IoT-Device Simatic IOT 2000


## Überblick
Dieses Dokument beschreibt die einmalige Einrichtung einer Simatic IOT 2000 um eine oder mehrere (IP-)Kameras an dingx anzubinden. In diesem Schritt wird noch keine Kamera angebunden, es nur das Setup ausgeführt welches es dem IoT-Device ermöglicht sich am AWS-Relay anzumelden, so dass das Gerät über einen Wartungszugang für North-Tec erreichbar ist und dann an den Kunden ausgeliefert werden kann.


## Anforderungen
Bevor dieser Schritt erfolgen kann muss der Neukunde zunächst, wie in `setup-camrelay-neukunde.md` beschrieben, angelegt werden.


## Kundenspezifischen SSH-Key installieren
Zuerst wird aus der [Download-Area des „camera-mjpeg-relay“-Projekts](https://bitbucket.org/biogaspiraten/camera-mjpeg-relay/downloads/) das Key-Archiv des Kunden, z.B. `id_northtec_testkunde_01-key.zip`, heruntergeladen. Es muss Files der folgenden Art enthalten:

- Private Key in `id_northtec_testkunde_01`.
- Public Key in `id_northtec_testkunde_01.pub`.

Die Files `id_northtec_testkunde_01` und `id_northtec_testkunde_01.pub` müssen auf
der Simatic-IoT in `/root/.ssh/` kopiert und dann mit den entsprechenden Rechteeinstellungen ausgestattet werden:  
`chmod 600 /root/.ssh/id_northtec_testkunde_01`

Ein Testaufruf von `ssh -i .ssh/id_northtec_testkunde_01 testkunde01@ec2-54-77-80-80.eu-west-1.compute.amazonaws.com`
sollte zu einer Meldung führen dass keine interaktive Shell auf dem AWS-Relay gestartet werden kann.


## AutoSSH installieren

Auf IoT-Seite sollte autossh vorhanden sein um den SSH-Tunnel wieder automatisch starten zu können. Prinzipiell kann eine Einrichtung auch ohne autossh stattfinden, falls aber jemals der SSH-Prozess beendet wird – z.B. durch DNS-Fehler o.ä. – muss dieser durch den Wartungszugang manuell neu gestartet werden.

Beispiel Setup unter yocto linux / Simatic:

```
cd /root/src
wget http://www.harding.motd.ca/autossh/autossh-1.4e.tgz
gunzip autossh-1.4e.tgz 
tar -xf autossh-1.4e.tar 
cd autossh-1.4e
./configure 
make
make install
autossh --version
```



## Wartungstunnel einrichten

/etc/init.d/launch-servicetunnel.sh:

```
#!/bin/sh
su root -c '/usr/local/bin/autossh -f -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -i /home/root/.ssh/id_northtec_testkunde_01 testkunde01@ec2-54-77-80-80.eu-west-1.compute.amazonaws.com -R 9000:127.0.0.1:22 -N'
```

```
update-rc.d launch-servicetunnel.sh defaults 80 10
```

Nach der Einrichtung des Tunnels sollte das IoT-Device einmal neu gestartet werden, z.B. via `reboot`-Kommmando, um sicherzustellen dass der autossh-Prozess nach einem Neustart tatsächlich verfügbar ist und die Verbindung korrekt aufbaut. Dies kann auf AWS-Seite mit folgendem Befehl getestet werden:

```
ssh -p 9000 root@localhost
```

Wenn nun das Passwort der Simatic abgefragt wird und mit diesem ein Login möglich ist, kann der Tunnel als funktionsfähig eingestuft werden.


## Abschluss

An diesem Punkt ist das Simatic-Device so eingerichtet dass es selbstständig immer eine SSH-Verbindung zum AWS-Relay herstellt und North-Tec so immer den Zugriff von außen ermöglicht. Eine so eingerichtete und getestete Simatic kann an den Kunden ausgeliefert werden. Die Anbindung der Kameras an die Simatic wird im nächsten Dokument beschrieben.



