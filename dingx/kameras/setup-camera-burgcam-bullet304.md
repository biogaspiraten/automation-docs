# Anbindung einer BurgWächter Burgcam 304 Bullet-Kamera

## Einrichten der ersten Kamera auf diesem IoT-Device
Falls noch kein Skript mit dem Namen `/etc/init.d/launch-camrelay.sh` auf dem IoT-Device existiert muss dieses mit dem folgenden Inhalt angelegt werden:

```
#!/bin/sh
su root -c '/usr/local/bin/autossh -f -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -i /home/root/.ssh/id_northtec_testkunde_01 testkunde01@ec2-54-77-80-80.eu-west-1.compute.amazonaws.com -R 9001:192.168.1.41:80 -N'
```

Dabei sind der SSH-Key von `testkunde_01`, der über den `-i`-Parameter angesprochen wird, sowie der Loginname `testkunde01@ec2…` entsprechend dem aktuellen Kunden anzupassen. Die IP-Adresse `192.168.1.41` und der Port `80` müssen auf die IP-Adresse der Kamera bzw. den entsprechenden Port umgewandelt werden.

Danach wird das Skript als „Autostart-Skript“ registriert, so dass es nach einem Neustart immer ausgeführt wird:

```
update-rc.d launch-camrelay.sh defaults 80 10
```

Damit ist die Einrichtung der Kamera abgeschlossen.

## Einrichten parallel zu bereits existierender Kamera auf diesem IoT-Device

Falls bereits ein Skript mit dem Namen `/etc/init.d/launch-camrelay.sh` auf dem IoT-Device existiert wird lediglich ein neuer `-R`-Parameter hinzugefügt. Beispiel:

Zusätzlich zum bereits bestehenden

```
-R 9001:192.168.1.41:80
```

– welches nicht abgeändert werden darf – wird ein weiterer Parameter dieser Form angehängt.  

```
-R 9001:192.168.1.41:80 -R 9002:192.168.1.42:80
```

Hierbei müssen die neue IP `192.168.1.42` bzw. der zugehörige Port `80` auf die Werte der neuen Kamera umgeändert werden.

Nach dem Abspeichern das Skripts sollte das IoT-Device neu gestartet werden. Dadurch wird zum einen der neue Tunnel aktiv, zum anderen kann nachvollzogen werden ob das Skript nach eine Neustart korrekt aktiv wird.

