##Build-Prozess

###Android
1. Task-Prozess ```gulp platformandroid``` aufrufen
2. ```cordova platform add android```


config.xml -> Plugin Section sieht dann wie folgt aus:*
```xml 
<plugin name="cordova-plugin-camera" spec="^4.0.3" />
<plugin name="cordova-plugin-file" spec="^6.0.1" />
<plugin name="cordova-plugin-file-transfer" spec="^1.7.1" />
<plugin name="cordova-plugin-vibration" spec="^3.1.0" />
<plugin name="cordova-plugin-geolocation" spec="^4.0.1" />
<plugin name="phonegap-nfc" spec="^1.0.3" />
<plugin name="phonegap-plugin-barcodescanner" spec="https://github.com/phonegap/phonegap-plugin-barcodescanner.git">
    <variable name="ANDROID_SUPPORT_V4_VERSION" value="25.+" />
</plugin>
<plugin name="cordova-plugin-network-information" spec="^2.0.1" />
<plugin name="cordova-plugin-whitelist" spec="^1.3.3" />
<plugin name="com-sarriaroman-photoviewer" spec="^1.1.18" />
<plugin name="cordova-plugin-document-viewer" spec="^0.9.10" />
<plugin name="cordova-plugin-dialogs" spec="^2.0.1" />
<plugin name="com.telerik.plugins.nativepagetransitions" spec="^0.6.5" />
<plugin name="cordova-plugin-device" spec="^2.0.2" />
<engine name="android" spec="^7.1.1" />
```
*Versionsnummer können sich mit der Zeit verändern

3. ```cordova platform build android```
4. folgende Plugins manuell nach installieren:
* cordova-plugin-firebase
* com-darryncampbell-cordova-plugin-intent //(für 2D-Scannerfunktion)
* net.m3mobile.scan*
*```cordova plugin add https://github.com/jaeyun0/cordova-plugin-m3scan```


Wenn die der Build-Prozess nicht erfolgreich verlief, dann müssen diese Plugins manuell installiert werden
Folgende Prozedur wird empfohlen
* Folgende Ordner löschen ```nodemodules, platforms, plugins```
* folgende Dateien löschen ```package-lock.json```
* Alle Plugineinträge in der ```config.xml, package.json``` löschen
* Einzelne Eintrage mit ```cordova plugin add \<pluginname>@\<version>* hinzufügen
*Versionen kann man aus den Repositories entnehmen
* nach jedem Hinzufügen sollte man den Build-Vorgang ```cordova build android``` ausführen



###iOS

1. Task-Prozess ```gulp platformios``` aufrufen
2. ```cordova platform add ios```


config.xml -> Plugin Section sieht dann wie folgt aus:*
```xml
<plugin name="cordova-plugin-whitelist" spec="^1.3.3" />
<plugin name="cordova-plugin-vibration" spec="^2.1.6" />
<plugin name="cordova-plugin-file-opener2" spec="^2.0.19" />
<plugin name="com-sarriaroman-photoviewer" spec="^1.1.11" />
<plugin name="cordova-plugin-document-viewer" spec="^0.9.7" />
<plugin name="cordova-plugin-file-transfer" spec="^1.7.0" />
<plugin name="cordova-plugin-geolocation" spec="^3.0.0" />
<plugin name="cordova-plugin-camera" spec="^4.0.3" />
<plugin name="cordova-custom-config" spec="^5.0.2" />
<plugin name="com.telerik.plugins.nativepagetransitions" spec="https://github.com/Telerik-Verified-Plugins/NativePageTransitions" />
<plugin name="cordova-plugin-file" spec="^5.0.0" />
<plugin name="cordova-plugin-network-information" spec="^2.0.1" />
<plugin name="phonegap-plugin-barcodescanner" spec="https://github.com/phonegap/phonegap-plugin-barcodescanner.git" />
<plugin name="cordova-plugin-geolocation" spec="^4.0.1" />
<plugin name="cordova-plugin-device" spec="^2.0.2" />
<plugin name="phonegap-nfc" spec="^1.0.3">
    <variable name="NFC_USAGE_DESCRIPTION" value="Read NFC Tags" />
</plugin>
<engine name="ios" spec="~4.5.5" />
```

3. Ordnerbrechtigungen auf dem MAC auf dem MAC-OS Terminal
```shell
sudo chmod -R 775 ./Nepaharius
sudo chown -R northtec ./Nepaharius
``` 

4. Plugin für Push-Benachrichtigungen installieren
```cordova plugin add phonegap-plugin-push```

5. Sicherstellen das die Ordner immernoch großzügig freigegeben sind:

```shell
cd ./Nepharius

sudo chmod -R 775 ./platforms
sudo chown -R northtec ./platforms

cd ./platforms/ios
pod setup
... prozess ...
pod install
```

6. Öffen der Datei ```./Nepahrius/platforms/ios/***.xcworkspace mit X-Code
7. Erforderliche Signatur einfügen
8. Build-Vorgang mit XCode auf Smartphone starten.