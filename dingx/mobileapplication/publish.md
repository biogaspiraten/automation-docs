###iOS
####iTunes Connect

**Typsche Fragen und Antworten aus dem Review**
- Does your app access any paid content or services?
    *Answer: No, there are no paid content.*

- What are the paid content or services, and what are the costs?
    *Answer: No, there are no paid content.*

- Do individual customers pay for the content or services?
    *Answer: No, The application is completely free.*

- If no, does a company or organization pay for the content or services? 
    *Answer: No, everyone can use it.*

- Where do they pay, and what's the payment method?
    *Answer: They do not pay for the content.*

- If users create an account to use your app, are there fees involved?
    *Answer: No fees are involved!*

- How do users obtain an account?
    *Answer: They can get an account, by contacting us by mail or by phone*

- Who is the target audience of the app?
    *Answer: This app is for everyone*

- Is this app meant for internal distribution in your own company, in the company of one target client, or in multiple target clients’ companies?
    *Answer: No, the app will be accessible to everyone.*

- In which countries will this app primarily be distributed?
    *Answer: For the start-up phase in Germany, and for all immigrants from other countries. -later worldwide.

- If this app is meant for internal distribution, will the app be accessible by both internal and external partners? Or will it be exclusive to in-house employees?
    *Answer: We do not plan any internal sales.