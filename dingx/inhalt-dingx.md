## Dingx 

* Dingx-connector einrichten
  * [Teil 1 - Den Sender installieren](./connector/connector-sender-installation.md)
  * [Teil 2 - Den Sender konfigurieren](./connector/connector-sender-konfiguration.md)
  * [Teil 3 - Den Empfänger installieren](./connector/connector-receiver-installation.md)
  
* Dingx-Serverlandschaft
  * [Hypervisor und VMs](./server/Hyper-V/hyper-v_inhalt.md)
  * [Telegraf](./server/Telegraf/telegraf_inhalt.md)
  * [DingX-Connector Senderüberwachung](./server/DingX_Connector_Senderueberwachung/inhalt.md)
  * [Prozessmanager PM2 für Autostart konfigurieren](./server/pm2Autostart.md)

* [Dingx-Webanwendung](./webapplication/Dingx-Dokumentation.md)

* [Dingx-Kameraanbindung](./kameras/README.md)

