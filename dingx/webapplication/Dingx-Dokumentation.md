##Style-Guide
###Javascript
[Airbnb Style Guide](https://github.com/airbnb/javascript)

####CSS
[Airbnb Style Guide](https://github.com/airbnb/css)
[BEM](https://en.bem.info/)

##North-Tec Style-Guide
##### Löschen von Dokumenten
Alle Dokumente sollen aufgrund der Redundanz und Abhängigkeiten zu Anderen Dokumenten nicht gelöscht werden. Stattdessen bekommt jedes Dokument einen Eintrag:
```json
{ 
    "deleted": {
        "_user_id": ObjectId(""), //Benutzer_id des Users der das Dokument gelöscht hat
        "datetime": ISODate(), //Zeitpunkt wann das Dokument gelöscht wurde
    }
}
```

##Betriebsmittelbuchung
####Datenbankschema
```json
{ 
    "_id" : ObjectId("5ad07152722d952244890d5a"), 
    "Bezeichnung" : "10'er Dusche", 
    "Seriennummer" : "1016005", 
    "Artikelnummer" : "", 
    "Zeitpunkt" : ISODate("2018-04-13T08:58:50.308+0000"), 
    "_asset_id" : ObjectId("5975b0944b9e962f740d3e85"), 
    "Projekt" : {
        "_id" : ObjectId("59677772abe0251c18e1bc4a"), 
        "Bezeichnung" : "Dingx"
    }, 
    "Kunde" : {
        "_id" : ObjectId("593f947d4b9e9633ac259a22"), 
        "Bezeichnung" : "Testfirma"
    }, 
    "location" : {
        "geometry" : {
            "coordinates" : [
                -80.522372, 
                43.465187
            ], 
            "type" : "Point"
        }, 
        "properties" : {
            "longitude" : "-80.522372", 
            "latitude" : "43.465187"
        }
    }, 
    "_user_id" : ObjectId("56f15f383bfcd06420c4476e")
}
```


##Dokumentenmanagement
###Allgemeines Datei-Object-Konventionschema innerhalb von Dokumenten
```json
"files" : [
        {
            "originalname" : {string} //Dateiname "1512553045031.jpg", 
            "encoding" : {string} //"7bit", 
            "mimetype" : {string} //"image/jpeg", 
            "destination" : {string} //"./ntfilestorage/593f947d4b9e9633ac259a22_Testfirma/docman/Erfasste Doks", 
            "fullpath" : "ntfilestorage/593f947d4b9e9633ac259a22_Testfirma/docman/Erfasste Doks/1512553045031.jpg", 
            "filesize" : {int} //NumberInt(515269), 
            "DateUploaded" : {Datetime} //ISODate("2017-12-06T09:49:20.822+0000"), 
            "UploadedbyUserId": {ObjectId},
            "tags": {array} 
        }, 
        {
            ...
        }
    ], 
```

##Aufgabenverwaltung


####Datenbankschema

```json
{
	"_id": {object} ObjectId("xxx123xxx"),
	"_parent_id": {string} //task id zur übergeortenten id der wenn Task-type == "group" oder bei 		
							intervall die übergeordnete sammelaufgabe
							
	"finishedDatetime": {Date} //ZEitpunkt an dem Tag wo die Aufgabe erledigt wurde
	"Bezeichnung": {string} //Bezeichnung der Aufgabe	
	"Kunde": {
				"Bezeichnung": //Name des Kunden,
				"_id: {string}: //("xxx123xxx"), // ObjectId des Kunden
			},
	"task-type": {string} //[single, group, repeated, triggered],	
	"tags": {array} [], // Array mit zugewiesen Tags
	"links": {array} [],
	"Bezug": {object}, //Kann das Linkschema aus den verschiedenen Gebieten sein
						//siehe /views/Generics/LinkForms/Stores/link-linkobjects-types.js
	"Beschreibung": {string},
	"Personal": {array} [ {
				"_id": {string},
				"Bezeichnung": {string},
				"gCalID": {string}
	},{...},{...} ],
	
	//rruleData ist null wenn "task-type" == group or triggerd or repeated
	//Recurrence Rule gemäß RFC2445 http://www.ietf.org/rfc/rfc2445.txt Page > 39
	//rruleData lehnt sich an rrecurjs
	"rruleData" : {
	    "dtstart": {isoDate}, //ist gleich "Soll-Beginn"
		"until": {isodate}, // bis zu welchem Datum sollen die Wiederholungen gehen
        "freq" : {string} "daily", 		// "secondly" / "minutely" / "hourly" / "daily" / "weekly" / "monthly" / "yearly"								
        "interval" : {integer},  //
		"count": {integer}, maximale Anzahl der Wiederholungen
        "bymonth" : {array},  // [1, 2, 8] Monate 1 === Januar und nicht wie typsch Javascript 
        "byweekno" : {array},  // [1, 2, 8] // Wochennummern
        "byyearday" : {array},  // [1, 2, 8] // Tag im Jahr
        "bymonthday" : {array},  // [1, 2, 8] // Tag im Monat
        "byweekday" : {array},  // [1, 2, 8] // Tag in der Woche
        "byhour" : {array},  // [1, 2, 8] // welche Stunde am Tag
        "byminute" : {array},  // [1, 2, 8] // welche Minute in der Stunde
        "bysecond" : {array},  // [1, 2, 8] // welche Sekunde in der Minute        
        "wkst" : {string},  //"SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"        
    }, 
    
	//isScheduled ist null wenn "task-type" == group or triggerd or repeated
	"isScheduled": {boolean},
    
	//isEventBased ist null wenn "task-type" == group or triggerd or repeated
	"isEventBased": {boolean},
    
	//event ist null wenn "task-type" == group or triggerd or repeated
	"event": {object} {
		"target-value": {string},
		"event-type": {array}, //["mqtt","database"] //möglichkeit der Erweiterung
		"Bezeichnung" {string},	//z.B. Betriebsstunden	
		"database": {string},//influx
		//Condition entsprechen der Mongo Comparison grundsetzlich {event-target.value} condition {mqtt oder database Wert} // isIncomming nur bei mqtt
		"condition": {string}, //["eq","gt","gte","lt","lte","ne","in","nin","isIncomming"] 
		
		"mqtt-path": {string},				
		
		"databasecfg": {object}, 	//{
									// "Bezeichnung" : "Betriebstunden", 
            						// "measurement" : "Separation", 
            						// "topic" : "nt/hansthun/Separator1/Betriebsstunden", 
            						// "timeRangeVal" : NumberInt(0), 
            						// "influxQuery" : "", 
            						// "Einheit" : "h", 
            						// "Korrekturfaktor" : "1/3600"
									//}
		
		
		//sie Assets Eigenschafteb Betriebsmittel
		"requestInterval_ms": {number}, 
		
	},
	"isInternal": {bool},
	"Soll-Zeitaufwand": {
            "totalminutes": {int},
			"minutes": {int},
            "totalhours": {float},
			"hours": {int},
			"days": {int},
			"weeks": {int},
			"years": {int}
	},
	"Ist-Zeitaufwand": {
			"totalminutes": {int},
			"minutes": {int},
			"totalhours": {float},
			"hours": {int},			
			"days": {int},			
			"weeks": {int},
			"years": {int}
	},
	
	"Soll-Beginn": {ISO-Date},
	"Soll-Ende": {ISO-Date},
	
	"Ist-Beginn": {ISO-Date}, //Für die Zeiterfassung
	"Ist-Ende": {ISO-Date},
	},
	"Zeiterfassung": {array} [{
		{
			"Start": {ISODate},
			"Ende": {ISO-Date},
			"Zeitdifferenz":{
									"totalminutes": {sint},
									"minutes": {int},			
									"totalhours": {float},
									"hours": {int},			
									"days": {int},
									"weeks": {int},
									"years": {int}			
								},
			"Beschreibung": {string},
			"Standort": {object} {
				"lng": {float},
				"lat": {float}			
			}
		}
	},{...},{...}],
	
	"trigger": {string}, //RFC iCal DURATION -P 	 // dur-week   = 1*DIGIT "W"
													  //dur-hour   = 1*DIGIT "H" [dur-minute]
													  //dur-minute = 1*DIGIT "M" [dur-second]
													  //dur-second = 1*DIGIT "S"
													  //dur-day    = 1*DIGIT "D"	
													  // -P15M für 15minuten im Vorraus

    "scheduleTrigger" : {string}, //ISO8601 Moment 2.3 //hilfreich um mit moment.js zu arbeiten, ansonsten richtet sich nach der gleichen Zeit wie bei trigger
	Benachrichtigung: {array}}[{		
		
		"priority": {int}, // 1=sofort, 2=in der nächsten folgenden Arbeitsstunde, 3 == Routine
		"type": {string} // ["call","sms","mail","broadcast"]		
		"mail-message": {string},
	    "sms-message": {string},
		"mqtt-message": {
				path: {string},
				message: {string}
		}
		},{...},{...}],
       
		
	"Benutzereingaben": {object} {
		"Statusreport": {Object} { 
            "comment": {string},
            "files": {array} // {gemaß Filekonvention}
        }
		"Messwertaufnahme": {object} {
                "isSet": false,
                //Anmerkung: alle Messwerte sollen nachher ins PRotokoll übertragen werden type="measurement-record"                                
                "measuredTagset":[{
                    "assetId":"5afee11bb3a8304f12aef9c5",
                    "assetName": "Pumpe",
                    "configId": "5afee11bb3a8304f12aef9c5",
                    "locationId": "5afee11345a8304f12aef1ad",
                    "locationName": "Bredstedt",
                    "longitude": "54.1234",
                    "latitude": "8.1233",
                    "projectId": "5afee1112348304f12aef111",
                    "projectName": "DingX",
                    "username": "Klaus Fakrim",
                    "userId": "5afee1112348304f12aef111",
                    "type":"manual",
                    "by": "app",
                    "unit": "kW",
                    "description": "Leistung"                                      
                }]
		},  
		"Tagerfassung":  {
                "isSet": false,
                "manual": true,
                "qrtag": "",
                "recordings": [
                    {                        
                        "datetime": new Date(),                        
                        "user": "Dev",
                        "location": {
                            "longitude": 0.0,
                            "latitude": 0.0
                        },
                        
                    }

                ]
            },
        "Kontrolle" : {Object} {
            "isSet": false,
            "isChecked" : {Boolean} //true, 
            "result" : {Boolean} // true, 
            "datetime" : {Datetime} //ISODate("2017-11-22T09:13:20.739+0000"), 
            "user" : {
                "_id" : {ObjectId} //ObjectId("599ff169a4b3382a749ee025"), 
                "Benutzername" : {string} // "kfakrim"
            }, 
            "location" : {object} {
                "longitude" : 8.9768613, 
                "latitude" : 54.6224366
            }
        }
	},
    "location" : {
        "geometry" : {
            "coordinates" : [
                6.934934018507207, 
                50.3267385974417
            ], 
            "type" : "Point"
        }, 
        "properties" : {
            "longitude" : "6.934934018507207", 
            "latitude" : "50.3267385974417"
        }
    }, 

	"process_type": "task", // damagereport, alarm, disorder    -> Vorgangstyp	
    "state": "open",  //accepted, inQueue, clarify, inProgress, done, deleted,
    "finishAt": [DateTime]
    "contractor" : {
        "_id" : ObjectId("56716ab61b2945582a027139"), 
        "Bezeichnung" : "North-Tec Maschinenbau GmbH"
    }, 
    "positionInQue" Int(), // Position in der Warteschlange
    "customID": 543
    
}

```

### Schemabeschreibung
##### task-type (string)
| Wert | Beschreibung |
|---|---|
| group | Aufgabengruppen enthalten Aufgabensammlungen |
|single| Einzelaufgabe |
|tiggered| wird durch eine Einzelaufgabe erzeugt, mit der Eigenschaft `isEventBased` |
|repeated| wird durch eine Einzelaufgabe erzeugt, mit der Eigenschaft `isScheduled` |
|generic| dieser Aufgabentyp kann eine Unterbrechung sein, z.B. Urlaub |

##### location (Object)
Das ""location" Object entspricht der Konvention für eine GEOJson Object der von MongoDB
indiziert werden kann. Dies ist der immer der Standortort der zuletzt von der App erfasst wurde.

### Benutzereingaben
##### Tagerfassung (object)
Beispiel
```json
"Tagerfassung":  {
                "isSet": false,
                "manual": true,
                "qrtag": "",
                "recordings": [
                    {                        
                        "datetime": new Date(),                        
                        "user": "Dev",
                        "location": {
                            "longitude": 0.0,
                            "latitude": 0.0
                        },
                        
                    }

                ]
            }
```
| Eigenschaft | Typ | Beschreibung |
|---|---|---|
|isSet|bool| zur Auswertung in der mobilen App |
| manual |bool| wird benötigt für die Auswertung in der mobilen App, bei `true` wir der Wert aus `qrtag` gelesen, bei `false` wird der QrCode aus dem Betriebsmittel ( Mongo ObjectId ) genommen |
| qrtag |string| Taginformation |
| recordings |object| Erfassung der Nutzerdaten wie Standort, Nutzer und Position


####Google Calendar

#####Authentifierierungs Schlüssel anlegen
1. Devleloperkonto unter https://console.cloud.google.com/ anlegen.
1. Projekt anlegen
1. Calendar API aktivieren und neue OAuth 2.0-Client-IDs in den Zugangsdaten erstellen.
1. Autorisierte Javascript-Quellen `http://portal2.north-tec.de` eintragen
1. Autorisierte Weiterleitungs-URIs `http://portal2.north-tec.de/googleAuth` eintragen
1. Schlüssel erzeugen
1. Schlüssel in die Datei "googleAPI-OAuth.js" in den Controllern einfügen.

---
## Manuelle Messwerterfassung

Datenformat für den AMQP Datenaustausch
```json
 [
    { 
        "measurement":"manual_metrics",
        "fields":{"value":42},
        "timestamp":1529401390582,
        "tags":{
                "assetId":"5afee11bb3a8304f12aef9c5",
                "assetName": "Pumpe",
                "locationId": "5afee11345a8304f12aef1ad",
                "locationName": "Bredstedt",
                "longitude": "54.1234",
                "latitude": "8.1233",
                "projectId": "5afee1112348304f12aef111",
                "projectName": "DingX",
                "username": "Klaus Fakrim",
                "userId": "5afee1112348304f12aef111",
                "type":"manual",
                "by": "app",
                "unit": "kW",
                "description": "Leistung"
            }
    }
]
```
---
##Nutzerverwaltung

####Datenbankschema
```json
{ 
    "_id" : ObjectId("56f15f383bfcd06420c4476e"), 
    "Kunde" : [
        "593f947d4b9e9633ac259a22"
    ], 
    "_kunde_id" : ObjectId("593f947d4b9e9633ac259a22"), 
    "Benutzername" : "Dev", 
    "Vorname" : "Detlefer", 
    "Nachname" : "Developer", 
    "Passwort" : "$2a$10$1QfquI/exzo0e.X.WQWy9.Z3eqZJP97OgF0TzKKho8VKEX4QPq3QW", 
    "isFreelance" : false, 
    "email" : "northtecbiogas@gmail.com", 
    "Personalnummer": "4832904",
    "Mandant": "GmbH & Co. KG", //{string} Mandentenbezeichnung - Initiator Beyersdorf,
    "Abrechnungskreis": "GEH", //{string}  Beyersdorf,
    "Abteilung": "ZV",
    "Tätigkeit" : "", 
    "expire" : ISODate("2017-03-23T14:57:50.000+0200"), 
    "role" : {
       
        "template" : "master", 
        "usergroup" : "TestAdmin", 
        "isEnabled" : true, 
        "isVisible_CustomerUserAdmin" : false
    }, 
    "visu-config" : {
        "key" : "san"
    }, 
    "influxDB" : "northtec", 
    "menu-config" : [
        {
            "text" : "Dashboards", 
            "href" : "#", 
            "icon" : "fa fa-th", 
            "nodes" : [
                {
                    "text" : "Login", 
                    "href" : "/GrafanaView?url=/login", 
                    "icon" : "fa fa-unlock-alt", 
                    "nodes" : [

                    ]
                }, 
                {
                    "text" : "Übersicht", 
                    "href" : "/GrafanaView?url=dashboard/snapshot/A46rE03P77SHBhInStwxquqdYa8Elv3b", 
                    "icon" : "fa fa-area-chart", 
                    "nodes" : [

                    ]
                }
            ]
        }, 
        {
            "text" : "Prozess-Board", 
            "href" : "/Aufgabenmanagement#overview", 
            "icon" : "far fa-clipboard-list", 
            "nodes" : [

            ]
        }, 
        {
            "text" : "Ressourcenkalender", 
            "href" : "/calendar", 
            "icon" : "fa fa-calendar", 
            "nodes" : [

            ]
        }, ...
              
       
    ], 
    "avatar" : {

    }, 
    "googleAuthData" : [], 
    "presentationAccounts" : [
        ObjectId("57a82d83f277ccb8f7521e3f"), 
        ObjectId("58aad855e5ae4b2ef00a829a"), 
        ObjectId("57ad8d035a4df0cd1a625093"), 
        ...
    ], 
    "isGeotrackable" : false, 
    "Tätigkeit" : "", 
    "mapMarkerPath" : "", 
    "isActive" : true, 
    "isVisible" : true, 
    "Ort" : {

    }, 
    "Projekt" : {
        "_id" : ObjectId("5976ed235667ffd81568d574"), 
        "Bezeichnung" : "Kerteminde"
    }, 
    "Foto" : "", 
    "Standort" : "", 
    "Telefon" : "", 
    "Zusatz" : "", 
    
    "isServiceLeader" : true, 
    "devices" : [
        {
            "uuid" : "34f0b21067e2b4f8", 
            "serial" : "5200934cb44d45f1", 
            "platform" : "Android", 
            "model" : "SM-T395", 
            "manufacturer" : "samsung", 
            "push_config" : {
                "android" : {
                    "token" : "cLJR0UepP6s:APA91bE3ltu8frpXtCHGVFLkM7oL9RFnAdUQcpFLFS6ozLUckRtGKUKZ51vu8fMRFm85YgKKRiBcs5ApTTImBM7SqUkFkdkYUz3_kwcSsN8FCDucG9sUHVO1nMlySAdQPCUELksj8Ijh"
                }
            }
        }, 
        
    ], 
    "isConsumer" : true
}

```

### Schemabeschreibung

###Benutzerrollen


| Rollenname | Rollenbeschreibung | Einschränkungen |
|------------|:-------------------|:-|
| ntadmin | Zugriff auf alle Einstellungen |
| nteditor | Zugriff auf Funktionen und mit erweiterten Rechten |
| ntuser | Zugriff auf alle stanard Funktionen | 
| testadmin | Entwicklerkonto ohne Einschränkungen |
| sales | Vertriebskonto mit allen EInstellung die fürs Marketing notwendig sind | keine Schreibrechte auf den Nutzerkonten | 
| owner | Besitzer des Kundenkontos |
| full | Normales Admin Konto auf Kundenebene |
| writer | Normales Benutzerkonto mit eingeschränkten Zugriff | keine Nutzer verwaltung 
| reader | Besitz nur Leserechte | keine Schreibrechte  |

###Einrichtungpasswörter

Kundenpasswort: Birgo#42Tram

###Release veröffentlichen


####Google API OAuth Daten anpassen
Dazu in der "googleAPI-OAuth.js" im Controller die Zeile module.exports = OAuthLocal; zu module.exports = OAuthLive; ändern.


```
#!js

//module.exports = OAuthLocal;
module.exports = OAuthLive;
```


####FTP verbindung herstellen
1. [Filezilla](https://filezilla-project.org/) eine Verbindung zum Server (IP-Adresse: 192.168.222.25) aufbauen
1. Benutzername __northtec__ und dem Standardpasswort anmelden.
1. Folgende Ordner vom Entwicklungsordner müssen auf den Server in **/home/northtec/nt_servers/NTOnlineWebApp** kopiert werden:
	__models, controllers, routes, views, /public/javavascripts, /public/stylesheets__
	bei Anderung in __/public/images__ und __/public/fonts__ die Order entsprechend mit kopieren
1. folgende Dateien sind zusätzlich bei Änderung notwendig: `app.js` und `package.json`
1. Putty
1. pm2


####Git-Book dnx-user-manual aktualisieren
1. Auf dem Server befindet sich das Gitbook in dem Pfad **/nt_servers/Dokumentation/dnx-usermanual**.
1. Zunächst das aktuelle Repository Lokal herunterladen und mit FileZilla auf den Server ziehen.
1. In den Pfad über die Console navigieren und **gitbook build** ausführen.
1. gitbook serve wird in diesem Fall nicht anschließend ausgeführt. Stattdessen wird der dnx-user-manual Prozess über die Console neu gestartet: **sudo pm2 restart 25** (Die "25" kann sich ändern und steht für die pid des dnx-user-manuell Prozesses, welche sich über sudo pm2 list erfahren lässt.)

[Airbnb Style Guide](./Dingx-Kundenanpassungen.html)