## SANI
###Betriebsmittelschema
Beispiel
```json
{ 
    "_id" : ObjectId("59097e8e179edafe0b29a648"), 
    "createdAt": new Date(),
    "Bezeichnung" : "Toilettenkabine Handicap", 
    "isMobileAsset" : true, 
    "Eigenschaften" : {
        "allgemein" : {
            "Artikelnummer" : "MB-BE-001", 
            "Seriennummer" : "KB0025"
        }
    }, 
    "_parent_id" : ObjectId("577d28515adc64bc5ea5dff8"), 
    "Kunde" : {
        "_id" : ObjectId("56726c41194d03c418a3550e"), 
        "Bezeichnung" : "SANI GmbH"
    }, 
    "Ort" : {
        "_id" : "577d28515adc64bc5ea5dff8", 
        "Bezeichnung" : "Lager Manningen"
    }, 
    "service" : {
        "clean" : {
            "_log_id" : null, 
            "Bemerkung" : "", 
            "letzter_Zeitpunkt" : {
                "Zeitpunkt" : ISODate("2017-04-05T13:13:58.978+0000"), 
                "Betriebsstunden" : NumberInt(0)
            }, 
            "nächster_Zeitpunkt" : {
                "Zeitpunkt" : ISODate("2017-04-11T13:13:58.000+0000"), 
                "Betriebsstunden" : NumberInt(72)
            }, 
            "Interval" : {
                "Zeitraum" : {
                    "years" : NumberInt(0), 
                    "months" : NumberInt(0), 
                    "weeks" : NumberInt(0), 
                    "days" : NumberInt(4), 
                    "hours" : NumberInt(0), 
                    "minutes" : NumberInt(0), 
                    "seconds" : NumberInt(0)
                }, 
                "Betriebsstunden" : NumberInt(72)
            }, 
            "Status" : {
                "gelb" : {
                    "Zeitraum_bevor_nächster_Zeitpunkt" : {
                        "seconds" : NumberInt(0), 
                        "minutes" : NumberInt(0), 
                        "hours" : NumberInt(12), 
                        "days" : NumberInt(0), 
                        "weeks" : NumberInt(0), 
                        "years" : NumberInt(0)
                    }, 
                    "Zeitpunkt" : ISODate("2017-04-11T01:13:58.000+0000"), 
                    "Betriebsstunden_bevor_nächster_Zeitpunkt" : NumberInt(0), 
                    "Betriebstunden" : NumberInt(72)
                }, 
                "rot" : {
                    "Zeitpunkt" : ISODate("2017-04-11T12:13:58.000+0000"), 
                    "Betriebsstunden_bevor_nächster_Zeitpunkt" : NumberInt(0), 
                    "Betriebstunden" : NumberInt(72), 
                    "Zeitraum_bevor_nächster_Zeitpunkt" : {
                        "seconds" : NumberInt(0), 
                        "minutes" : NumberInt(0), 
                        "hours" : NumberInt(1), 
                        "days" : NumberInt(0), 
                        "weeks" : NumberInt(0), 
                        "years" : NumberInt(0)
                    }
                }
            }
        }, 
        "refill" : {
            "_log_id" : null, 
            "Bemerkung" : "", 
            "letzter_Zeitpunkt" : {
                "Zeitpunkt" : ISODate("2017-04-28T08:58:37.625+0000"), 
                "Betriebsstunden" : NumberInt(0)
            }, 
            "nächster_Zeitpunkt" : {
                "Zeitpunkt" : ISODate("2017-06-16T08:58:37.000+0000"), 
                "Betriebsstunden" : NumberInt(72)
            }, 
            "Interval" : {
                "Zeitraum" : {
                    "years" : NumberInt(0), 
                    "months" : NumberInt(0), 
                    "weeks" : NumberInt(7), 
                    "days" : NumberInt(0), 
                    "hours" : NumberInt(0), 
                    "minutes" : NumberInt(0), 
                    "seconds" : NumberInt(0)
                }, 
                "Betriebsstunden" : NumberInt(72)
            }, 
            "Status" : {
                "gelb" : {
                    "Zeitraum_bevor_nächster_Zeitpunkt" : {
                        "seconds" : NumberInt(0), 
                        "minutes" : NumberInt(0), 
                        "hours" : NumberInt(12), 
                        "days" : NumberInt(0), 
                        "weeks" : NumberInt(0), 
                        "years" : NumberInt(0)
                    }, 
                    "Zeitpunkt" : ISODate("2017-06-15T20:58:37.000+0000"), 
                    "Betriebsstunden_bevor_nächster_Zeitpunkt" : NumberInt(0), 
                    "Betriebstunden" : NumberInt(72)
                }, 
                "rot" : {
                    "Zeitpunkt" : ISODate("2017-06-16T07:58:37.000+0000"), 
                    "Betriebsstunden_bevor_nächster_Zeitpunkt" : NumberInt(0), 
                    "Betriebstunden" : NumberInt(72), 
                    "Zeitraum_bevor_nächster_Zeitpunkt" : {
                        "seconds" : NumberInt(0), 
                        "minutes" : NumberInt(0), 
                        "hours" : NumberInt(1), 
                        "days" : NumberInt(0), 
                        "weeks" : NumberInt(0), 
                        "years" : NumberInt(0)
                    }
                }
            }
        }
    }, 
    "Template" : {
        "_id" : "5909856de5ae4b2b900500d6"
    }, 
    "_template_id" : ObjectId("5909856de5ae4b2b900500d6")
}
```