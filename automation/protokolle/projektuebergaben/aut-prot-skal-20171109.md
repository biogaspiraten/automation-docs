# Projektübergabe Skals

> Teilnehmer: Jan, Bahne, Björn R., André, Lennard, Max, Jörn, Peter, Hauke, Nils

> Protokoll: Björn Riekenberg

> Datum: 09.11.2017

> Projekt: 84/17

>AB: 305/17 (Maschinenbau), ? (Automation)

>Projektordner: Y:/Dansk_Energieadgivning/Kielsen-Jensen-Skals


## BHKWs
Gasproduktion von 800 Nm³ soll in 1x 1,5 MW Jenbacher in Skals umgesetzt werden, der über VPN an die BGA angebunden werden soll. Am BHKW soll eine lokale SPS gesetzt werden, wie in Deinstedt. Als Schnittstelle zum BHKW soll Profibus genutzt werden.

## Behälter 
Fermenter, Nachgärer: 3 x Paddelgigant, 3 x Amaprop 1000
Es wird Fe3Cl in die Behäter dosiert. Dafür kommt ein eigenstädiges Modul zum Einsatz, dem ein Sollwert für die Menge vorgegeben werden soll. Die Ansteuerung der Dosierung muss noch geklärt werden  (Impulse, Analogwert, etc.). Es soll kein Jobspooler eingesetzt werden, sondern eine festgelegte Anzahl von Intervallen mit einer einstellbaren Menge.


## Technikgebäude
NT9000-Standard, bis auf die Wärmerückgewinnung

## Technikhalle
Wird vom Kunden gebaut. Hier werden Separator und Biogrinder + Twin untergebracht

## Fütterung
Biogrinder (Martensen), Unterbauschnecke, Steigschnecke, Börger Twin, Pumpe Bigmix

## Separator
NT1000 mit Förderband wird von Zentralepumpe aus NT9000 versorgt und hat eine eigene Dünnphasenpumpe

## Heizung
Wärmerückgewinnung aus der Gülle, die vom Nachgärer ins Endlager gepumpt wird, inkl. Wärmepumpe.
Um Strovid (Harnstoff) Ablagerungen im Wärmetauscher zu verhindern, wird Kemguard per Dosierpumpe ins System dosiert (140 l pro Tag).

## Gaskühlung, Gasverdichter, Aktivkohle

Verdichter 2x 7,5 kW Himmel sollen von aus auf den Gasdruck am BHKW gesteuert werden, d.h. die Verdichter werden über FU gesteuert. 







