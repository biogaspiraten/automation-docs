## Gespräch am 26.10.2017, Umzug und Hosting der Webseite im eigenen Bereich

## Teilnehmer
 * Timo Jautzim , Thomas Hartmann, Hauke Czora, Christian Sönksen

## Inhalt
- Server soll im xxx.xxx.66.xxx Bereich gehostet werden
- IP-Adressbereiche müssen eventuell neu zugeordnet werden
    - Vermeidung von Konflikten beim Einrichten von Testgeräten
- nur der Shop soll über https gehostet werden
    - https://www.north-tec.de:443  wird vom Mail-Server gebraucht    
    - Probleme beim Routen für ssl-Zertifikate

Hauke: Kümmert sich um die Beschaffung der SSL-Zertifikate, sowie das Testen des Zugangs
            