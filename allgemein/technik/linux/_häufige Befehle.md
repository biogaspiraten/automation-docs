####[sudo](https://wiki.ubuntuusers.de/sudo/)
 - sudo - Alles als Root(Admin) - ausführen

####[bash](https://wiki.ubuntuusers.de/Bash/)
- sudo bash - Immer als Root in der Kommandozeile bleiben

####[nano](https://wiki.ubuntuusers.de/nano/)
 - Texteditor

####[nginx](https://wiki.ubuntuusers.de/nginx/)
- lauscht auf dem Port 80 und gibt bestimmte Services auf den Routen frei

```bash
sudo service nginx restart
```
- neustart des Proxyservers für alle Webbasierten anwendungen

####[node]
```bash
node eineJavascriptDatei.js
```
- startet eine Node.js Javascript Datei
