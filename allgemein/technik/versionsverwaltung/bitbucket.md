## Versions Controlsystem mit Git und Bitbucket

### Voraussetzung
1. eigenes Konto bei [Bitbucket](https://bitbucket.org/)
1. Administrator muss dieses Konto für das jeweilige Repository freigeben

#### Tools

#####Windows und Linux
[git](https://git-scm.com)

##### Windows
[Tortoise Git mit Language Pack](https://tortoisegit.org/download/)

####Git-Clone
Mit Git-Clone bekommt man das Remote-Repository auf seine lokale Festplatte kopiert. 
![Git Clone Tutorial](images/git-clone.gif)

####Git-commit
Mit Git-Commit werden alle Änderungen in das lokale Repository gespeichert.
Jetzt befindet sich die Änderung im HEAD, aber noch nicht im entfernten Repository.
Bei jedem Commit muss man darauf achten das auch neuhinzugefügte Dateien mit übernimmt.
Die "Message" muss eingeben werden. Sie beschreibt kurz das wesentliche was der aktuelle Commit beinhaltet.

####Git-push
Die aktuellen Änderungen befinden sich im HEAD, diese müssen jetzt mit dem Remote-Repository abgeglichen werden.
Dies geschieht über der git-push Befehl. Grundsätzlich werden alle Änderungen auf dem master-Branch getätigt.
Sollten bei der Entwicklung mehrer Personen gleichzeitigt beteiligt sein, empfielt es sich spezielle Features auf einem eigenen Branch auszulagern.


![Git Clone Tutorial](images/git-push.gif)


####Git-pull
