#Publish and Deploy
#Git Server für Windows
[Bonobo Git Server](https://bonobogitserver.com/)
#Version 1
##Linux Server
```sh
$ mkdir -p /home/git/project_name.git
$ cd /home/git/project_name.git
$ git init --bare
```

```sh
#!/bin/sh 
GIT_WORK_TREE=/var/www/project_name git checkout -f
```


```sh
$ chmod +x hooks/post-receive
```

```sh
ssh me@myserver
cd repository/.git

sudo chmod -R g+ws *
sudo chgrp -R mygroup *

git config core.sharedRepository true

```

```sh
sudo chown root:git -R webhooks/
```

##Client
```sh
ssh://git@192.168.66.179/opt/gitserver.git
```

#Version 2
##Cron Job

```ssh
0 7-18 * * 1-5  root    /home/git/project_name/script.sh
```

```ssh
#!/bin/bash
cd /home/git/project_name
git fetch
git reset --hard origin/master
git pull --progress -v --no-rebase --ff-only --no-commit "origin"
pm2 restart dingx-automation-docs
```

#Version 3
webhook



