##IP Adressenzuordnung für die Arbeitsplatze

Adressraum: 192.168.66.0/16

Arbeitsplatzaufteilung

----
|*|200-209|
|-:|-:|
|140-149|170-179|
|||
|*|150-159|
|160-169|*|

Frei: 
- 130-139
- 180-189
- 190-199 ?


#####Entwickler-WLAN Access Point DHCP
192.168.66.21 - 129

#####TCP/IPv4 Einstellung
Gatway: 192.168.222.11
DNS: 192.168.222.6 