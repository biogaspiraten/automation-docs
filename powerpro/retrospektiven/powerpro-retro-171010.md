# Retrospektive vom 10.Oktober.2017

## Keep

* Probleme gemeinsam als Team lösen
* Alle Scrum-Meetings sollen weiter durchgeführt werden

## Drop

* Der Sprint wird abgeschafft und stattdessen wird nach einem Kanban-Board gearbeitet. __DONE__

## Try

* Neue Mitarbeiter sollen zusammen mit erfahrenen Entwicklern an einem Projekt arbeitet, um die Struktur des Systems besser kennen zu lernen (Pair-Programming)

* Alle neuen Aufgaben werden in dem Kanban-Board erfasst.

* Das Kanban-Board soll um eine Spalte 'Review' ergänzt werden. Alle fertigen Aufgaben kommen erst in diese Spalte, werden dann im Review dem Team und dem PO vorgestellt und erst danach als 'Fertig' eingestuft. In dem Review soll bei Bedarf auch der Code zu der Aufgabe gezeigt werden und das Team hat noch einmal die Möglichkeit Fragen dazu zu stellen und Anregungen zu geben, bevor die Aufgabe abgeschlossen wird. __DONE__

* Thore Nomensen sollte auch Zugriff auf Jira bekommen und Boxen für Regelenergie direkt in das Board eintragen.

