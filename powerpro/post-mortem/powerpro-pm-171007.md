# Post-mortem zum 7.Oktober.2017 - Ausfall der Modbus TCP Schnittstelle

## Ablauf

Am Samstag den 07.10 um 05:11 Uhr hat es 18 Verbindungsabbrüche gegeben. Um 05:37 hat Christian Jungclaus Stephan Sandow darüber informiert. Stephan konnte die anliegenden Verbindungsabbrüche beheben. Christian hat Stephan darauf aufmerksam gemacht, dass der Leitstand zwischen 07:00-19:00 nicht besetzt ist.
Um 16:28 macht Sören Jensen den Leistand und das PowerPro-Team per E-Mail auf die Verbindungsprobleme aufmerksam und bittet um Unterstützung. Sören wiederholt diese Bitte per E-Mail um 20:37. Am Montag den 09.10 ist aufgefallen, dass der E-Mail-Server von North-Tec keine Mails zugestellt hat.
Zum Schichtbeginn um 19:00 Uhr haben 26 Verbindungsabbrüche angelegen. Die Meldungen lagen seit 08:15 Uhr an. Stephan Sandow wurde informiert. Er konnte die Verbindungsabbrüche beheben. Um 21 Uhr lagen erneut mehrere Verbindungsabbrüche an, über die Stephan Sandow informiert wird. Um 20:50 macht Torge Wendt in der WhatsApp Gruppe 'PowerPro Nordgröön' auf die Verbindungsabbrüche aufmerksam - und erhält keine Antwort. Um 21:51 erkündigt sich Ralf in der WhatsApp - Gruppe vom PowerPro Team nach, ob dieses Problem gelöst ist - und erhält keine Antwort. Um ca. 22 Uhr hat sich Ralf Breckling telefonisch beim Leitstand erkundigt, ob es aktuell Probleme gibt und dran gearbeitet. Ralf hat Christian die Anweisung gegeben, dass Stephan sich falls nötig Hilfe bei dem Problem holen soll. Christian hat drauf hin mit Stephan telefoniert. Stephan hat entschieden Lars am nächsten Morgen zu informieren. Über die Nacht ist es mehrmals zum gleichen Problem gekommen. Sobald das Problem angelegen hat, hat Christian Stephan informiert und er konnte die Verbindungsabbrüche sofort wieder beheben. Zum Schichtende 07:00 hat Christian noch mal mit Stephan telefoniert und angekündigt das der Leitstand bis 19:00 nicht besetzt ist.

Am Sonntag den 08.10 um 16:30 beklagt sich Sören per Mail beim Leitstand und dem Team über die fehlende Rückmeldung und liefert Details zu den Verbindungsabbrüchen. Zum Schichtbeginn um 19:00 hat Christian einen Anruf von Lars Stankuweit bekommen. Lars hat sich der Sache angenommen. Stephan hat ihn morgens informiert. Lars hat die SPS der Modbus TCP - Schnittstelle neu starten lassen. Über die Nacht hat es keine weiteren Vorfälle gegeben. Das Problem schient behoben zu sein.

## Indentifizierete Schwachstellen

* Die Verbindungsüberwachung für die Schnittstellen Modbus und Beckhoff hat nicht richtig funktioniert und dadurch wurde die Größe des Problems nicht erkannt.

* Es war für den Leitstand nicht ausreichend sichtbar, dass die Live-Daten nicht mehr geschrieben werden.

* Die Telefonnummer des Leitstandes und des Programmierers im Notdienst war nicht ausreichend bekannt, so dass zwischen Nordgröön und North-Tec nur per E-Mail und WhatsApp kommuniziert wurde.

* Der Leitand war nicht in die beiden WhatsApp-Gruppen aufgenommen und konnte daher nicht auf die dortigen Anfragen reagieren.

## Maßnahmen

* Der Fehler in der Verbindungsüberwachung soll behoben werden. Status: `Erledigt`

* Es soll in Grafana ein Dashboard erstellt werden, dass für jede Schnittstelle ein Digramm mit der aktuellen summierten Leistung enthält. Zusätzlich zur Verbindungsüberwachung soll der Leitstand dieses Diagramm regelmaäßig kontrollieren. Status: `In Arbeit`

* Um die Erreichbarkeit vom Leitstand und des Programmierers im Notdienst zu erhöhen, soll es eine 'Kontakt'-Seite im PowerProControlCenter geben. Status: `In Arbeit`

* Der Leitstand soll in die WhatsApp-Gruppen aufgenommen werden. Status: `Erledigt`
