# Dokumentation der North-Tec Automation GmbH

23.Oktober 2017 - Björn Riekenberg

Dies ist die Dokumentation der North-Tec Automation GmbH. Hier findest du die Dokumentation zu unseren Prozessen und Arbeitsabläufen, Protokolle zu Teammeetings und jede Menge Tutorials zu technischen Themen.

> Diese Dokumentation lebt davon, dass sie von allen gepflegt und erweitert wird. Wenn es also ein spezielles Thema gibt, mit dem du dich besondern gut auskennst, schreibe eine Dokumentation, so dass auch andere in der Firma in die Lage versetzt werden, diese Aufgabe zu übernehmen.

Einzelne Dokumente werden in [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) erstellt, dazu empfehlen wir [Visual Studio Code](https://code.visualstudio.com/), weil es schlank und einfach zu lernen ist, außerdem  kann man es noch für viele andere Sprachen nutzen. Der Quellcode dieser Dokumentation liegt in unserer Versionsverwaltung [Bitbucket](https://bitbucket.org/biogaspiraten/automation-docs) als repositroy vor. Du solltest dir eine lokale Kopie davon erstellen, dann kannst du Änderungen bequem lokal machen und anschließend wieder hochladen. Wie du das machst, erklärt dieses Tutorial: [Versionsverwaltung - Git mit Bitbucket](/allgemein/technik/versionsverwaltung/bitbucket.md)

## Inhalt

### [1. Allgemein](/allgemein/README.md)

### [2. Automation](/automation/README.md)

### [3. Ding.X](/dingx/inhalt-dingx.md)

### [3. PowerPro](/powerpro/README.md)
