# North-Tec Automation Dokumentation

## [Allgemein](/allgemein/README.md)

* Prozesse (Arbeitsabläufe)
  * [Bearbeitung von Projekten und Arbeitsaufträgen mit dem Kanban-Board](/allgemein/prozesse/kanban/kanban1.md)
  
* Technik
  * [Backup und Restore - Anlagen PC mit Acronis aufsetzen](/allgemein/technik/backup/acronis.md)
  * [Versionsverwaltung - Git mit Bitbucket und Visual Studio Code](/allgemein/technik/versionsverwaltung/bitbucket.md)
  * [Datentransfer - Der ftp-Server](/allgemein/technik/datentransfer/ftp.md)
  * Umgang mit der Linux-Platform
    * [Häufige Befehle](/allgemein/technik/linux/_häufige Befehle.md)

## [Automation](/automation/README.md)

* Protokolle
  * [Projektübergabe 84/17 - Skals](./automation/protokolle/projektuebergaben/aut-prot-skal-20171109.md)
* SPS
  * [Die Symbole für Ein- und Ausgänge anlegen - Vom Schaltplan in die SPS](/automation/sps/sps-symbolik.md)
  * [Einen Sensor in ein Programm einfügen](/automation/sps/sps-sensor.md)
  * [Einen Antrieb in ein Programm einfügen](/automation/sps/sps-antrieb.md)
* Sensoren
  * [Radar- und Ultraschallsensoren von Vega parametrieren - VEGASON und VEGAPULS](/automation/sensoren/vega-fuellstand.md)
  * [Grenzstandsensoren von Vega parametrieren - VEGACAP](/automation/sensoren/vega-grenzstand.md)
  * [Durchflusssensoren von Krohne parametrieren](/automation/sensoren/krohne-durchfluss.md)
  * [Drucksensoren von ifm parametrieren](/automation/sensoren/ifm-druck.md)
* Antriebe
  * [SINAMICS G120 von Siemens mit Starter in Betrieb nehmen und optimieren](/automation/frequenzumrichter/siemens-sinamics.md)
* Energiemessung
  * [Das PAC3200 in Betrieb nehmen und mit einer S7-1500 auslesen](/automation/energiemessung/siemens-pac3200.md)
  * [Das Energymeter der ET200sp in Betrieb nehmen und mit einer S7-1500 auslesen](/automation/energiemessung/siemens-et200sp.md)
* Wärmemengenzähler
  * [Mehrere M-Bus-Zähler mit dem Scanner von Wachendorff in Betrieb nehmen](/automation/mbus/mbus-inbetriebnahme.md)
  * [M-Bus-Zähler mit dem Gateway von Wachendorff auslesen](/automation/mbus/mbus-inbetriebnahme.md)
  * [Das M-Bus-Gateway von Wachendorff mit einer S7-1200 auslesen](/automation/mbus/mbus-inbetriebnahme.md)
* BHKWs
  * [Anbindung eines Jenbacher-BHKWs über Profibus an eine S7-1500](/automation/mbus/mbus-inbetriebnahme.md)
* Alarmmanagement
  * [Störmeldungen in der SPS anlegen](/automation/alarmmanagement/alarm-sps.md)
  * [Störmeldungen im NTadmin anlegen](/automation/alarmmanagement/alarm-ntadmin.md)
  * [eWON für die Alarmierung konfigurieren](/automation/alarmmanagement/alarm-ewon.md)
  * [Das Meldesystem in WinCC Professional konfigurieren](/automation/alarmmanagement/alarm-wincc.md)
  * [Die Tag/Nach-Schaltung in WinCC Professional konfigurieren](/automation/alarmmanagement/alarm-tagnacht.md)
* Energiemanagement

## [Ding.X](./dingx/README.md)

* [Inhalt](./dingx/inhalt-dingx.md)

* Dingx-connector einrichten
  * [Teil 1 - Den Sender installieren](/dingx/connector/connector-sender-installation.md)
  * [Teil 2 - Den Sender konfigurieren](/dingx/connector/connector-sender-konfiguration.md)
  * [Teil 3 - Den Empfänger installieren](/dingx/connector/connector-receiver-installation.md)
  
* Dingx-Serverlandschaft
  * [Hypervisor und VMs](/dingx/server/Hyper-V/hyper-v_inhalt.md)
  * [Prozessmanager PM2 für Autostart konfigurieren](/dingx/server/pm2Autostart.md)

* [Dingx-Webanwendung](/dingx/webapplication/Dingx-Dokumentation.md)


## [PowerPro](./powerpro/README.md)

* Insys Connectivity Service
  * Neuen Router einrichten


* Verbindungen
  * [Modbus Tcp](/powerpro/verbindungen/powerpro-con-modbustcp.md)
  * [IEC61580-5-104](/powerpro/verbindungen/powerpro-con-modbustcp.md)
  * [Enercon](/powerpro/verbindungen/powerpro-con-modbustcp.md)
  * [Enertrag](/powerpro/verbindungen/powerpro-con-modbustcp.md)
  * [Senvion](/powerpro/verbindungen/powerpro-con-modbustcp.md)

* Retrospektiven
  * [10.10.17](/powerpro/retrospektiven/powerpro-retro-171010.md)

* Post-Mortems
  * [07.10.2017 - Ausfall der ModbusTcp-Schnittstelle](./powerpro/post-mortem/powerpro-pm-171007.md)

* Memos
  * [26.10.2017 - Publizieren der North-tec Hompage](/allgemein/memos/20171026_publish-nthomepage.md)
